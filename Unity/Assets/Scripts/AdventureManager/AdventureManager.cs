﻿using UnityEngine;
using System.Collections;
using Utage;

public class AdventureManager : SingletonMonoBehaviour<AdventureManager> {

	[SerializeField]
	GameObject advCanvas;

	// Use this for initialization
	protected override void Awake () {
		base.Awake();
		StartCoroutine(StartDelayed());
	}

	void Start () {
		GameManager.Instance.OnLoadSceneAdventure(this);
	}

	IEnumerator StartDelayed () {
		UICanvas before = UICanvas.Instance;
		if(before == null) yield break;
		before.AddPrefab("AdvEngineStarter", "Prefabs/AdvEngineStarter");	// ADVシーンでエンジンをセットアップするプレハブ
		before.AddPrefab("Canvas", "Prefabs/Canvas");	// 元のシーンで使うCanvas

		ModelTakedOverCanvas model = before.CreateModel();
		Transform tmp = before.Base;
		tmp.gameObject.SetActive(false);
		tmp.SetParent(null, false);
		DebugText.Instance.transform.SetParent(tmp, false);

		Destroy(before.gameObject);
		while(before != null) yield return 0;

		UICanvas canvas = advCanvas.AddComponent<UICanvas>();
		canvas.SetParamByModel(model, advCanvas.GetComponent<RectTransform>());
		advCanvas.AddComponent<UIController>(); // Loadのためだけに
		tmp.SetParent(canvas.Base, false);
		tmp.gameObject.SetActive(true);
		DebugText.Instance.transform.SetParent(canvas.Base, false);

		GameObject go = Instantiate(canvas.GetPrefab("AdvEngineStarter")) as GameObject;
		//manager.AddDontDestroy(go);
		AdvEngineStarter starter = go.GetComponent<AdvEngineStarter>();
		string scenarioLabel = SetScenarioLabel();
		starter.StartEngine(scenarioLabel);
		StartCoroutine(CheckEndScenario(starter.Engine));
	}

	IEnumerator CheckEndScenario (AdvEngine engine) {
		while(!engine.IsEndScenario) yield return 0;

		DebugText.Instance.Log("Scenario Ended!");
		JumpScene(SetNextScene());
	}

	void JumpScene (UnityEngine.Events.UnityAction callback) {
		callback();
	}

	UnityEngine.Events.UnityAction SetNextScene () {
		switch(GameManager.Instance.PreviousScene) {
			case Scene.SelectEnemy:
				return (() => StartCoroutine(GoSceneMain()));
			case Scene.Result:
				return (() => StartCoroutine(GoSceneBase()));
			default:
				DebugText.Instance.Log("SceneTransitionError");
				break;
		}
		return (() => StartCoroutine(GoSceneBase()));
	}

	string SetScenarioLabel () {
		MonsterInfo monster = PlayerInfo.Instance.SelectedMonster;
		switch(GameManager.Instance.PreviousScene) {
			case Scene.SelectEnemy:
				return monster.BeforeBattleLabel;
			case Scene.Result:
				return monster.AfterBattleLabel;
			default:
				DebugText.Instance.Log("SceneTransitionError");
				break;
		}
		return "";
	}

	public IEnumerator GoSceneMain () {
		yield return StartCoroutine(InstantiateCanvas());
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Main", 0.1f);
	}

	public IEnumerator GoSceneBase () {
		yield return StartCoroutine(InstantiateCanvas());
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Base", 0.1f);
	}

	IEnumerator InstantiateCanvas () {
		UICanvas before = UICanvas.Instance;
		if(before == null) yield break;

		ModelTakedOverCanvas model = before.CreateModel();
		Transform root = before.Base.FindChild("Base");
		root.gameObject.SetActive(false);
		root.SetParent(null, false);
		DebugText.Instance.transform.SetParent(null, false);
		GameObject prefab = before.GetPrefab("Canvas");
		Destroy(before.gameObject);

		while(before != null) yield return 0;

		GameObject go = Instantiate(prefab) as GameObject;
		UICanvas canvas = go.GetComponent<UICanvas>();
		canvas.SetParamByModel(model, canvas.transform.FindChild("Base").GetComponent<RectTransform>());
		foreach(Transform t in root) {
			t.SetParent(canvas.Base, false);
			t.gameObject.SetActive(true);
		}
		DebugText.Instance.transform.SetParent(canvas.transform, false);
	}
}
