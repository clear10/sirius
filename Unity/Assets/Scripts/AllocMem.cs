﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

[ExecuteInEditMode()]
public class AllocMem : MonoBehaviour {

	public bool show = true;
	public bool showFPS = false;
	public bool showInEditor = false;

	public Text txt;
	public FPSCounter fps;

	void Start () {
		txt = GetComponent<Text>();
		useGUILayout = false;
		fps = new FPSCounter();
	}

	void Update () {
		if(!show || (!Application.isPlaying && !showInEditor)) {
			return;
		}

		int collCount = System.GC.CollectionCount(0);

		if(lastCollectNum != collCount) {
			lastCollectNum = collCount;
			delta = Time.realtimeSinceStartup - lastCollect;
			lastCollect = Time.realtimeSinceStartup;
			lastDeltaTime = Time.deltaTime;
			collectAlloc = allocMem;
		}

		allocMem = (int)System.GC.GetTotalMemory(false);

		peakAlloc = allocMem > peakAlloc ? allocMem : peakAlloc;

		if(Time.realtimeSinceStartup - lastAllocSet > 0.3F) {
			int diff = allocMem - lastAllocMemory;
			lastAllocMemory = allocMem;
			lastAllocSet = Time.realtimeSinceStartup;

			if(diff >= 0) {
				allocRate = diff;
			}
		}

		StringBuilder text = new StringBuilder();

		text.Append("Currently allocated\n		");
		text.Append((allocMem / 1000000F).ToString("0"));
		text.Append("mb\n");

		text.Append("Peak allocated ");
		text.Append((peakAlloc / 1000000F).ToString("0"));
		text.Append("mb_\n	(last collect ");
		text.Append((collectAlloc / 1000000F).ToString("0"));
		text.Append(" mb)\n");


		text.Append("Allocation rate\n		");
		text.Append((allocRate / 1000000F).ToString("0.0"));
		text.Append("mb\n");

		text.Append("Collection frequency\n		");
		text.Append(delta.ToString("0.00"));
		text.Append("s\n");

		text.Append("Last collect delta\n		");
		text.Append(lastDeltaTime.ToString("0.000"));
		text.Append("s(");
		text.Append((Time.timeScale / lastDeltaTime).ToString("0.0"));
		text.Append(" fps)");

		if(showFPS) {
			fps.Update();
			text.Append("\nFPS\n		");
			text.Append(fps.Current.ToString("0.0") + " fps");
		}

		txt.text = text.ToString();
	}

	private float lastCollect = 0;
	private float lastCollectNum = 0;
	private float delta = 0;
	private float lastDeltaTime = 0;
	private int allocRate = 0;
	private int lastAllocMemory = 0;
	private float lastAllocSet = -9999;
	private int allocMem = 0;
	private int collectAlloc = 0;
	private int peakAlloc = 0;

}

/**
 * FPS計測クラス
 * UnityにおいてFPSの制御が可能なのかは微妙なので計測だけ
 * 過去nフレームのログから算出するほうがいいかもしれない
 */
public class FPSCounter {
	float updateInterval;	// 更新される頻度
	float accumulated;
	float remainingTime;	//  次の更新までの残り時間
	int frameCount;

	float fps;

	public float Current {
		get { return fps; }
	}

	public FPSCounter (float interval = 1.0f) {
		updateInterval = interval;
		accumulated = 0.0f;
		remainingTime = updateInterval; //  次の更新までの残り時間
		frameCount = 0;
		fps = 0.0f;
	}

	public void Update () {
		remainingTime -= Time.deltaTime;
		accumulated += Time.timeScale / Time.deltaTime;
		++frameCount;
		if(remainingTime <= 0.0) {
			// FPSの計算
			fps = accumulated / frameCount;

			remainingTime = updateInterval;
			accumulated = 0.0F;
			frameCount = 0;
		}
	}
}