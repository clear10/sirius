﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseManager : SingletonMonoBehaviour<BaseManager> {

	// Use this for initialization
	void Start () {
		//GameManager.Instance.SendMessage("OnLoadSceneBase", this, SendMessageOptions.DontRequireReceiver);
		//DebugText.Instance.Log("BaseMgr->Start");
		GameManager.Instance.OnLoadSceneBase(this);
		InstantiatePartner();
		MissionManager.Instance.CheckAchieved();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void InstantiatePartner () {
		//DebugText.Instance.Log("InstantiatePartner");
		PlayerInfo player = PlayerInfo.Instance;
		//DebugText.Instance.Log("InstantiatePartner->progress" + player.GameProgress);

		UnitInfo info = player.GetRandomUnitInfo();
		//DebugText.Instance.Log("InstantiatePartner->info" + info.GetID());
		string path = info.GetSpritePath();
		path = path.Replace("icon", "home");
		//DebugText.Instance.Log("InstantiatePartner->path" + path);

		List<PhraseInfo> phrases = ImportMtbPhrases();
		if(phrases != null)
			phrases = (phrases.Count >0) ? phrases.FindAll((p) => (("Unit" + p.unitId.ToString("00")) == info.GetID())) : null;
		//DebugText.Instance.Log("phrases: " + phrases.Count);

		Sprite sp = Resources.Load<Sprite>(path);
		//DebugText.Instance.Log("Sprite: " + sp);
		GameObject obj = new GameObject("Partner_" + info.GetName());
		obj.transform.position = new Vector3(0, -0.1f, 0);
		SpriteRenderer sr = obj.AddComponent<SpriteRenderer>();
		sr.sprite = sp;
		BoxCollider2D col = obj.AddComponent<BoxCollider2D>();
		col.center.Set(0, 0);
		col.size.Set(2f, 8f / 3f);
		PartnerIcon icon = obj.AddComponent<PartnerIcon>();
		icon.SetPhrases(phrases);
	}

	List<PhraseInfo> ImportMtbPhrases () {
		Entity_Phrases entityPhrase = Resources.Load("Data/mtb_phrases") as Entity_Phrases;
		List<PhraseInfo> phrases = new List<PhraseInfo>();
		for(int i = 0; i < entityPhrase.sheets[0].list.Count; i++) {
			Entity_Phrases.Param data = entityPhrase.sheets[0].list[i];
			PhraseInfo info = new PhraseInfo(data.id, data.unit_id, data.text);
			phrases.Add(info);
		}
		return phrases;
	}

	public void GoSceneOrganize () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Organization", 0.1f);
	}

	public void GoSceneSelectEnemy () {
		if(!PlayerInfo.Instance.IsOrganized) {
			DebugText.Instance.Log("Did not organized!!");
			return;
		}

		UICanvas canvas = UICanvas.Instance;
		//canvas.ForceDestroy("BasePanel");
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("SelectEnemy", 0.1f);
		//DebugText.Instance.Log("GoSceneBattle");
	}

	public void GoSceneBook () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Book", 0.1f);
	}

	public void GoSceneDuty () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Duty", 0.1f);
	}
}
