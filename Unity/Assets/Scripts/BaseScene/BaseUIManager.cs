﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BaseUIManager : UIController {

	Text phraseText;
	[SerializeField]
	GameObject phraseObj;

	protected override void RegistPrefabs (UICanvas canvas) {
		canvas.AddPrefab("CommonButton", "Prefabs/UI/CommonButton", true);
		if(canvas.GetDontDestroyedObj("BasePanel") == null) canvas.AddPrefab("BasePanel", "Prefabs/UI/BasePanel");
	}

	protected override void ShowUI (UICanvas canvas, Transform parent) {
		GameObject pnlBase = canvas.GetDontDestroyedObj("BasePanel");
		if(pnlBase == null) {
			pnlBase = Instantiate(canvas.GetPrefab("BasePanel")) as GameObject;
			pnlBase.transform.SetParent(parent, false);
			pnlBase.name = "BasePanel";
			canvas.AddDontDestroy(pnlBase);
		}

		ShowPhraseUI(parent);

		BaseManager manager = GetComponent<BaseManager>();
		/*
		RectTransform rect = pnlBase.transform.GetChild(0).GetComponent<RectTransform>();
		rect.anchoredPosition = Vector2.zero;
		rect = pnlBase.transform.GetChild(1).GetComponent<RectTransform>();
		rect.anchoredPosition = new Vector2(185, 0);
		rect = pnlBase.transform.GetChild(2).GetComponent<RectTransform>();
		rect.anchoredPosition = new Vector2(-185, 0);
		 */
		InitButton(pnlBase.transform.FindChild("CommonButton3").gameObject, "図鑑", pnlBase.transform, () => manager.GoSceneBook());
		InitButton(pnlBase.transform.FindChild("CommonButton2").gameObject, "編成", pnlBase.transform, () => manager.GoSceneOrganize());
		InitButton(pnlBase.transform.FindChild("CommonButton0").gameObject, "戦闘", pnlBase.transform, () => manager.GoSceneSelectEnemy());
		InitButton(pnlBase.transform.FindChild("CommonButton1").gameObject, "任務", pnlBase.transform, () => manager.GoSceneDuty());
		InitButton(pnlBase.transform.FindChild("CommonButton4").gameObject, "←", pnlBase.transform, null);
		pnlBase.transform.FindChild("CommonButton4").gameObject.SetActive(false);
	}

	public void ShowPhraseUI (string txt) {
		if(phraseObj == null) return;
		if(!phraseObj.activeInHierarchy) ShowPhraseUI(UICanvas.Instance.Base);

		phraseText.text = txt;
	}
	void ShowPhraseUI (Transform parent) {
		if(phraseObj.activeInHierarchy) return;
		phraseObj = Instantiate(phraseObj, phraseObj.transform.position, phraseObj.transform.rotation) as GameObject;
		phraseObj.transform.SetParent(parent, false);
		phraseText = phraseObj.transform.GetChild(0).GetComponent<Text>();
	}
}
