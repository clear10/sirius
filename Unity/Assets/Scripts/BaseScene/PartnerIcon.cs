﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class PartnerIcon : MonoBehaviour, IPointerClickHandler {

	Transform trans;
	List<PhraseInfo> phrases;

	int tapCount = 0;
	float lastTapTime = 0;

	// Use this for initialization
	void Start () {
		trans = this.transform;
		StartCoroutine(FlowAnimation());
	}

	public void SetPhrases (List<PhraseInfo> infos) {
		phrases = infos;
		UpdatePhrase();
	}

	public void OnPointerClick (PointerEventData eventData) {
		const float interval = 0.4f;
		if(Time.time - lastTapTime < interval || tapCount == 0) {
			tapCount++;
		}
		else {
			tapCount = 1;
		}
		lastTapTime = Time.time;
		if(tapCount >= 10) {
			GoSceneDebug();
			return;
		}
		UpdatePhrase();
	}

	void UpdatePhrase () {
		if(phrases == null) return;
		string s = phrases[Random.Range(0, phrases.Count)].text;
		(BaseManager.Instance.GetComponent<BaseUIManager>()).ShowPhraseUI(s);
	}

	public void GoSceneDebug () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Debug", 0.1f);
	}
	
	// Update is called once per frame
	void Update () {
		//trans.Translate(new Vector3(0, Mathf.Sin(Time.time) * 1.0e-03f, 0));
	}

	IEnumerator FlowAnimation () {
		while(true) {
			trans.Translate(new Vector3(0, Mathf.Sin(Time.time) * 2.0e-04f, 0));
			yield return null;
			yield return null;
			yield return null;
		}
	}

	void OnDestroy () {
		StopAllCoroutines();
		trans = null;
	}
}
