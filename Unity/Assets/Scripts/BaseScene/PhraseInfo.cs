﻿using UnityEngine;
using System.Collections;

public class PhraseInfo {

	public readonly int id;
	public readonly int unitId;
	public readonly string text;

	public PhraseInfo (int id, int unit_id, string text) {
		this.id = id;
		this.unitId = unit_id;
		this.text = text;
	}
}
