﻿using UnityEngine;
using System.Collections;

public class BookManager : SingletonMonoBehaviour<BookManager> {

	// Use this for initialization
	void Start () {
		GameManager.Instance.OnLoadSceneBook(this);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void GoSceneOrganize () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Organization", 0.1f);
	}

	public void GoSceneSelectEnemy () {
		if(!PlayerInfo.Instance.IsOrganized) {
			DebugText.Instance.Log("Did not organized!!");
			return;
		}

		UICanvas canvas = UICanvas.Instance;
		//canvas.ForceDestroy("BasePanel");
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("SelectEnemy", 0.1f);
		//DebugText.Instance.Log("GoSceneBattle");
	}

	public void GoSceneBase () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Base", 0.1f);
	}

	public void GoSceneDuty () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Duty", 0.1f);
	}
}
