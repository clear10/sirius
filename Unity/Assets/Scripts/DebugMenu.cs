﻿using UnityEngine;
using System.Collections;

public class DebugMenu : SingletonMonoBehaviour<DebugMenu> {

	public void DeleteAllPlayerData () {
		PlayerPrefs.DeleteAll();
		DebugText.Instance.Log("削除完了: 再起動してください");
		DialogManager.Instance.ShowSubmitDialog(
			"ゲームを終了します",
			(bool result) =>
			{
				if(result)
					GameManager.Instance.QuitGame();
			}
		);
	}

	public void CleanCache () {
		UICanvas canvas = UICanvas.Instance;
		canvas.CleanAll();
		DebugText.Instance.Log("掃除完了: タイトルへ戻ってください");
	}

	public void DeleteMissionFile () {
		bool ret = MissionManager.Instance.DeleteMissionFile();
		if(ret)
			DebugText.Instance.Log("削除完了: タイトルへ戻ってください");
		else
			DebugText.Instance.Log("削除失敗: clear10に報告してください");
	}

	public void GoSceneTitle () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Title");
	}

}
