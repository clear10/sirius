﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugText : SingletonMonoBehaviour<DebugText> {

	Text txt;
	[SerializeField]
	AllocMem memStatus;
	static bool created = false;

	protected override void Awake () {
		txt = GetComponent<Text>();
		base.Awake();
		if(!created) {
			DontDestroyOnLoad(Instance.gameObject);
			created = true;
			return;
		}
		Destroy(this.gameObject);
	}

	public void Log (object message) {
		//if(!txt.enabled) return;
		if(txt == null) txt = GetComponent<Text>();
		string s = txt.text;
		if(s == "") {
			s = message as string;
		}
		else {
			string[] lines = s.Split('\n');
			int i = 0;
			if(lines.Length > 10) i = 1;
			for(s = null; i < lines.Length; i++) {
				s += lines[i];
				s += "\n";
			}
			s += message as string;
		}
		txt.text = s;
		Debug.Log(message);
	}

	public void SetShowText (Toggle t) {
		txt.enabled = t.isOn;
		memStatus.GetComponent<Text>().enabled = t.isOn;
		memStatus.show = t.isOn;
	}

	public void ResetParent(Transform t, bool flag) {
		transform.SetParent(t, flag);
	}
}
