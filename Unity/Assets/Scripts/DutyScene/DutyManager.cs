﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DutyManager : SingletonMonoBehaviour<DutyManager> {

	// Use this for initialization
	void Start () {

		GameManager.Instance.OnLoadSceneDuty(this);
		DutyUIManager ui = GetComponent<DutyUIManager>();

		List<Mission> missions = MissionManager.Instance.GetChallengingMission();
		if(missions == null || missions.Count == 0) {
			DebugText.Instance.Log("MissionEmpty:" + ((missions==null) ? "Null" : "0"));
			ui.AddNode2Content(null);
			return;
		}
		foreach(Mission m in missions) {
			ui.AddNode2Content(m);
		}
	}

	// Update is called once per frame
	void Update () {

	}

	public void GoSceneOrganize () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Organization", 0.1f);
	}

	public void GoSceneSelectEnemy () {
		if(!PlayerInfo.Instance.IsOrganized) {
			DebugText.Instance.Log("Did not organized!!");
			return;
		}

		UICanvas canvas = UICanvas.Instance;
		//canvas.ForceDestroy("BasePanel");
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("SelectEnemy", 0.1f);
		//DebugText.Instance.Log("GoSceneBattle");
	}

	public void GoSceneBase () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Base", 0.1f);
	}

	public void GoSceneBook () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Book", 0.1f);
	}
}
