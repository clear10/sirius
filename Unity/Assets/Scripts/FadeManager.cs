﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeManager : SingletonMonoBehaviour<FadeManager> {
	GameObject black;//暗転用のオブジェクト
	float time = 0.1f;//頻繁に使用するつもりのフェードイン/アウトの時間
	private static bool created = false;

	protected override void Awake () {
		base.Awake();
		if(!created) {
			DontDestroyOnLoad(this.gameObject);
			created = true;
			return;
		}
		Destroy(this.gameObject);
	}

	//秒数を指定したいときはこちらを呼び出す
	public void LoadLevel (string scene, float interval) {
		StartCoroutine(TransScene(scene, interval));
	}
	//timeの秒数を利用するとき
	public void LoadLevel (string scene) {
		StartCoroutine(TransScene(scene, time));
	}

	private IEnumerator TransSceneOut (string scene, float interval) {
		float time = 0;
		RawImage raw = SetFadeObject();

		while(time <= interval) {
			raw.color = new Color(0, 0, 0, Mathf.Lerp(0f, 1f, time / interval));
			time += Time.deltaTime;
			yield return false;
		}

		Application.LoadLevel(scene);

		//名前が変わる＝シーンが変わるのを待つ
		while(Application.loadedLevelName != scene) yield return false;
	}

	private IEnumerator TransSceneIn (string scene, float interval) {
		float time = 0;
		RawImage raw = SetFadeObject();
		//LoadLevelでCanvasが一度破棄されているはずなのでもう一度呼び出す
		//raw = SetFadeObject();

		while(time <= interval) {
			raw.color = new Color(0, 0, 0, Mathf.Lerp(1f, 0f, time / interval));
			time += Time.deltaTime;
			yield return false;
		}
		//Debug.Log("hoge");
		while(UICanvas.Instance == null) yield return false;
		//UICanvas.Instance.AddDontDestroy(black);
		UICanvas.Instance.ForceDestroy(black.name);
		//Destroy(black);
	}

	private IEnumerator TransScene (string scene, float interval) {

		/** hoge
		float time = 0;
		RawImage raw = SetFadeObject();

		while(time <= interval) {
			raw.color = new Color(0, 0, 0, Mathf.Lerp(0f, 1f, time / interval));
			time += Time.deltaTime;
			yield return 0;
		}

		Application.LoadLevel(scene);

		//名前が変わる＝シーンが変わるのを待つ
		while(Application.loadedLevelName != scene) yield return 0;

		time = 0;
		//LoadLevelでCanvasが一度破棄されているはずなのでもう一度呼び出す
		//raw = SetFadeObject();

		while(time <= interval) {
			raw.color = new Color(0, 0, 0, Mathf.Lerp(1f, 0f, time / interval));
			time += Time.deltaTime;
			yield return 0;
		}

		while(UICanvas.Instance == null) yield return 0;
		//UICanvas.Instance.AddDontDestroy(black);
		UICanvas.Instance.ForceDestroy(black.name);
		//Destroy(black);
		 */
		
		UICanvas.Instance.Loaded = false;
		yield return StartCoroutine(TransSceneOut(scene, interval));
		while(UICanvas.Instance == null) yield return new WaitForEndOfFrame();
		while(!UICanvas.Instance.Loaded) yield return false;
		yield return StartCoroutine(TransSceneIn(scene, interval));

	}

	//暗転用オブジェクトを生成し、透明度を変更するためRawImageを返す
	RawImage SetFadeObject () {
		string key = "FadeObject";
		GameObject obj = UICanvas.Instance.GetDontDestroyedObj(key);
		if(obj != null) {
			return obj.GetComponent<RawImage>();
		}

		black = new GameObject();
		black.name = key;
		//Canvasの名前やヒエラルキーを変更している場合は要修正
		black.transform.SetParent(UICanvas.Instance.Base);
		RectTransform rect = black.AddComponent<RectTransform>();
		rect.anchorMax = new Vector2(1f, 1f);
		rect.anchorMin = new Vector2(0f, 0f);
		rect.anchoredPosition = new Vector2(0, 0);

		UICanvas.Instance.AddDontDestroy(black);

		return black.AddComponent<RawImage>();
	}
}