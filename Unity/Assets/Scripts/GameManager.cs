﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : SingletonMonoBehaviour<GameManager> {

	private static bool created = false;

	int screenWidth;
	int screenHeight;

	Scene previous = Scene.None; // dafault;
	Scene current = Scene.Title; // dafault;

	public Scene PreviousScene { get { return previous; } }

	protected override void Awake () {
		base.Awake();
		if(!created) {
			DontDestroyOnLoad(this.gameObject);
			created = true;
			return;
		}
		Destroy(this.gameObject);
	}

	// Use this for initialization
	void Start () {
		DialogManager.Instance.SetLabel("Yes", "No", "Close");
		Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		DebugText.Instance.Log("screenX=" + screenWidth + ", screenY=" + screenHeight);
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.A)) ReturnTitleScene();
		if(Input.GetKeyDown(KeyCode.S)) QuitGame();
		if(Application.platform != RuntimePlatform.Android) return;
		if(Application.loadedLevelName == "Main") return;
		//if(Input.GetKeyDown(KeyCode.A)) ReturnTitleScene();
		//if(Input.GetKeyDown(KeyCode.S)) QuitGame();
		if(Input.GetKeyDown(KeyCode.Escape)) {
			switch(Application.loadedLevelName) {
				case "Organization":
					PlayerInfo player = GetComponent<PlayerInfo>();
					bool flag = OrganizeManager.Instance.CallSaveOrg(player);
					if(flag) player.Save();
					goto case "Base";
				case "Book":
				case "SelectEnemy":
				case "Duty":
				case "Base":
					DialogManager.Instance.ShowSelectDialog(
						"タイトルに戻りますか？",
						(bool result) =>
						{
							if(result)
								ReturnTitleScene();
						}
					);
					break;
				case "Title":
					DialogManager.Instance.ShowSelectDialog(
						"ゲームを終了しますか？",
						(bool result) =>
						{
							if(result)
								QuitGame();
						}
					);
					break;
				default:
					return;
			}
		}
	}

	public void QuitGame () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		Application.Quit();
#if UNITY_EDITOR
		if(Application.platform == RuntimePlatform.WindowsEditor)
			UnityEditor.EditorApplication.isPlaying = false;
#endif
	}

	void ReturnTitleScene () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.ForceDestroy("BasePanel");
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Title");
	}

	void UpdateSceneLog (Scene scene) {
		previous = current;
		current = scene;
	}

	public void OnLoadSceneOrganize (OrganizeManager om) {
		UpdateSceneLog(Scene.Organize);
		PlayerInfo player = GetComponent<PlayerInfo>();
		player.PrepareSceneOrganize(om);
	}

	public void OnLoadSceneBase (BaseManager bm) {
		UpdateSceneLog(Scene.Base);
		MissionManager.Instance.CheckAchieved();
	}

	public void OnLoadSceneSelectEnemy (SelectEnemyManager sem) {
		UpdateSceneLog(Scene.SelectEnemy);
	}

	public void OnLoadSceneDuty (DutyManager dm) {
		UpdateSceneLog(Scene.Duty);
		MissionManager manager = MissionManager.Instance;
		manager.ShowGetAwardDialog();
	}

	public void OnLoadSceneBook (BookManager bm) {
		UpdateSceneLog(Scene.Book);
	}

	public void OnLoadSceneAdventure (AdventureManager am) {
		UpdateSceneLog(Scene.Adventure);
	}

	public void OnLoadSceneResult (ResultManager rm) {
		UpdateSceneLog(Scene.Result);
	}

	public void OnLoadSceneMain (MainManager mm) {
		UpdateSceneLog(Scene.Main);
		PlayerInfo player = GetComponent<PlayerInfo>();
		player.PrepareSceneMain(mm);
	}

	public Vector2 GetScreenSize () {
		if(Application.platform == RuntimePlatform.Android) {
			DebugText.Instance.Log("Android");
		}
		else if(Application.platform == RuntimePlatform.WindowsEditor) {
			DebugText.Instance.Log("Editor");
		}
		//return new Vector2(720, 1280);
		return UICanvas.Instance.Size;
	}
}

public enum Scene {
	Title,
	Base,
	Organize,
	SelectEnemy,
	Book,
	Main,
	Result,
	Adventure,
	Duty,

	// 追加

	None,
};
