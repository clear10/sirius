﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AttackPanel : MonoBehaviour {

	public void SetPanel (Sprite _sprite, string _name, int damage) {
		//transform.SetParent(UICanvas.Instance.Base);
		Image icon = transform.FindChild("icon").GetComponent<Image>();
		icon.sprite = _sprite;
		Text attackTxt = transform.FindChild("attackTxt").GetComponent<Text>();
		attackTxt.text = _name + "の攻撃！";
		Text damageTxt = transform.FindChild("damageTxt").GetComponent<Text>();
		damageTxt.text = damage.ToString("00") + "のダメージ！";
	}
}
