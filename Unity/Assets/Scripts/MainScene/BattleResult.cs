﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleResult : MonoBehaviour {

	public int passedTurn = 0;			//経過ターン数
	public int myMaxDamage = 0;		//自分達が与えた最大ダメージ
	public string myMaxDamageID = "";	//そのユニットID
	public int yourMaxDamage = 0;		//自分達が受けた最大ダメージ
	public string yourMaxDamageID = "";//そのユニットID
	public bool isPlayerWin = false;	//プレイヤーが勝利したか
	public string strikerID = "";		//とどめを刺したユニットのID
	public bool isJustKill = false;	//ジャストダメージを与えて勝利したか
	public List<UnitInfo> deadUnits = new List<UnitInfo>();	//戦闘中死亡したユニットの情報


	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this.gameObject);
	}

	// Update is called once per frame
	void Update () {

	}

	public bool Save (Hashtable hash) {
		foreach(string key in hash.Keys) {
			switch(key) {
				case "passedTurn":
					passedTurn = (int)hash[key];
					break;
				case "myMaxDamage":
					myMaxDamage = (int)hash[key];
					break;
				case "myMaxDamageID":
					myMaxDamageID = (string)hash[key];
					break;
				case "yourMaxDamage":
					yourMaxDamage = (int)hash[key];
					break;
				case "yourMaxDamageID":
					yourMaxDamageID = (string)hash[key];
					break;
				case "isPlayerWin":
					isPlayerWin = (bool)hash[key];
					break;
				case "strikerID":
					strikerID = (string)hash[key];
					break;
				case "isJustKill":
					isJustKill = (bool)hash[key];
					break;
				case "deadUnitsID":
					string idstr = (string)hash[key];
					if(idstr == "") break;
					string[] IDs = idstr.Split(',');
					foreach(string _id in IDs) {
						UnitInfo info = PlayerInfo.Instance.GetUnitInfo(_id);
						if(info == null) {
							DebugText.Instance.Log("Dead unit not found: " + _id);
							return false;	//Failure
						}
						
						deadUnits.Add(info);
					}
					break;
				default:
					DebugText.Instance.Log("KeyError: " + key);
					return false;	//Failure
			}
		}
		return true;	//Success
	}

	public void ShowDebug () {
		PlayerInfo pi = PlayerInfo.Instance;
		DebugText debug = DebugText.Instance;
		debug.Log("passedTurn: " + passedTurn);
		debug.Log("myMaxDamage: " + myMaxDamage + "(" + pi.GetUnitInfo(myMaxDamageID).GetName() + ")");
		debug.Log("yourMaxDamage: " + yourMaxDamage + "(" + pi.GetUnitInfo(yourMaxDamageID).GetName() + ")");
		debug.Log("isPlayerWin: " + isPlayerWin);
		string nm = isPlayerWin ? pi.GetUnitInfo(strikerID).GetName() : "";
		debug.Log("striker: " + nm);
		debug.Log("isJustKill: " + isJustKill);
		string diedNames = "";
		for(int i = 0; i < deadUnits.Count; i++) {
			UnitInfo info = deadUnits[i];
			if(diedNames == "") diedNames = info.GetName();
			else diedNames += "," + info.GetName();
		}
		debug.Log("Dead: " + diedNames);

	}
}
