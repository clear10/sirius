﻿using UnityEngine;
using System.Collections;

public class CutIn : SingletonMonoBehaviour<CutIn> {

	public GameObject attackPrefab;
	public Transform[] transforms;

	[SerializeField]
	Transform leftDown;
	[SerializeField]
	Transform leftUp;
	[SerializeField]
	Transform downLeft;
	[SerializeField]
	Transform downRight;

	Vector3 center;

	public enum Side {
		Left,
		Right,
		Up,
		Down,
	};

	// Use this for initialization
	void Start () {
		//Vector3 leftTop = Camera.main.ScreenToWorldPoint(Vector3.zero);
		//Vector3 rightBottom = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
		//center = (rightBottom - leftTop) / 2;
		center = Camera.main.transform.position;
		center.z = 0;
	}

	// Update is called once per frame
	void Update () {
		/*
		if(Input.GetKeyDown(KeyCode.F)) {
			StartCoroutine(StartCutIn(60));
		}
		 */
	}

	public void StartCutIn (GameObject prefab, Side from, Side side, int frame, bool isUI = false) {
		// なんかいれとく
		Vector3 start = Vector3.zero;
		Vector3 goal = Vector3.zero;

		switch(from) {
			case Side.Left:
			case Side.Right:
				if(side == Side.Left || side == Side.Right) return;
				start.x = from == Side.Left ? leftDown.position.x : center.x - leftDown.position.x;
				goal.x = from != Side.Left ? leftDown.position.x : center.x - leftDown.position.x;
				start.y = side == Side.Down ? leftDown.position.y : leftUp.position.y;
				goal.y = start.y;
				break;
			case Side.Up:
			case Side.Down:
				if(side == Side.Up || side == Side.Down) return;
				start.x = side == Side.Left ? downLeft.position.x : downRight.position.x;
				goal.x = start.x;
				start.y = from == Side.Down ? downLeft.position.y : center.y - downLeft.position.y;
				goal.y = from != Side.Down ? downLeft.position.y : center.y - downLeft.position.y;
				break;
		}

		GameObject cutin = null;
		if(isUI) {
			cutin = prefab;
			RectTransform rect = cutin.GetComponent<RectTransform>();
			rect.SetParent(UICanvas.Instance.Base);
			rect.localScale = Vector3.one;
			Vector2 px = MainManager.Instance.PixelSize;
			rect.anchoredPosition = new Vector2(start.x * px.x, start.y * px.y);
		}
		else {
			cutin = Instantiate(prefab, start, Quaternion.identity) as GameObject;
		}
		StartCoroutine(CutInCoroutine(cutin, goal, frame, isUI, start));
	}

	/*
	public void StartCutIn (GameObject prefab, Side LR, Side UD, int n) {
		this.side_lr = (LR == Side.Left) ? Side.Left : Side.Right;
		this.side_ud = (UD == Side.Down) ? Side.Down : Side.Up;
		this.prefab = prefab;
		StartCoroutine(StartCutIn(n));
	}
	*/

	float EaseOutCubic (float t, float b, float c, float d) {
		t /= d;
		t = t - 1;
		return c * (t * t * t + 1) + b;
	}

	float EaseInCubic (float t, float b, float c, float d) {
		t /= d;
		return c * t * t * t + b;
	}

	IEnumerator CutInCoroutine (GameObject instance, Vector3 goal, int frame, bool isUI = false, Vector3 first = new Vector3()) {
		int f = 0;
		if(frame < 20) f = 50;
		else f = (frame - 10) / 2;

		Vector3 start = !isUI ? instance.transform.position : first;
		float x = 0;
		float y = 0;

		Vector2 ofs = Vector2.one;
		RectTransform rect = null;
		if(isUI) {
			ofs = MainManager.Instance.PixelSize;
			rect = instance.GetComponent<RectTransform>();
			//goal = new Vector3(goal.x * ofs.x, goal.y * ofs.y, goal.z);
		}

		for(int i = 0; i < f; i++) {
			x = EaseOutCubic((float)i / (float)f, start.x, (goal.x - start.x) / 2f, 1f);
			y = EaseOutCubic((float)i / (float)f, start.y, (goal.y - start.y) / 2f, 1f);
			if(!isUI) instance.transform.position = new Vector3(x * ofs.x, y * ofs.y, start.z);
			else {
				rect.anchoredPosition = new Vector2(x * ofs.x, y * ofs.y);
			}
			yield return (i == f-1) ? "Center" : "Moving";
		}
		
		yield return StartCoroutine(WaitForFrames(10));
		float _x = x;
		float _y = y;
		for(int i = 0; i < f; i++) {
			x = EaseOutCubic((float)i / (float)f, _x, goal.x - start.x, 1f);
			y = EaseOutCubic((float)i / (float)f, _y, goal.y - start.y, 1f);
			if(!isUI) instance.transform.position = new Vector3(x * ofs.x, y * ofs.y, start.z);
			else {
				rect.anchoredPosition = new Vector2(x * ofs.x, y * ofs.y);
			}
			yield return "Moving";
		}
		
		Destroy(instance);
	}

	/*
	IEnumerator StartCutIn (int n) {
		int frame = 0;
		if(n < 20) frame = 50;
		else frame = (n - 10) / 2;
		int s = this.side_lr == Side.Left ? 0 : 1;
		int j = this.side_ud == Side.Down ? 1 : -1;
		Vector3 start = transforms[s].position;
		GameObject obj = (GameObject)Instantiate(prefab, start, Quaternion.identity);
		float height = Mathf.Abs(obj.transform.position.y) * j;
		for(int i = 0; i < frame; i++) {
			obj.transform.position = new Vector3(start.x, EaseOutCubic((float)i / (float)frame, j* start.y, height, 1f), start.z);
			yield return 0;
		}
		yield return StartCoroutine(WaitForFrames(10));
		start.y = obj.transform.position.y;
		for(int i = 0; i < frame; i++) {
			obj.transform.position = new Vector3(start.x, EaseInCubic((float)i / (float)frame, start.y, height, 1f), start.z);
			yield return 0;
		}
		Destroy(obj);
	}
	 */

	IEnumerator WaitForFrames (int n) {
		for(int i = 0; i < n; i++) {
			yield return 0;
		}
	}
}
