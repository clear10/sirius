﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DamageEffect : MonoBehaviour {

	[SerializeField]
	Image bg;
	[SerializeField]
	Text txt;
	[SerializeField]
	Outline outline;
	[SerializeField]
	CanvasGroup canvasGroup;

	// Use this for initialization
	void Awake () {
		this.gameObject.SetActive(false);
	}

	public void StartAnimation (int damage, int frame) {
		SetDamageText(damage);
		this.gameObject.SetActive(true);
		StartCoroutine(AnimationCoroutine(frame));
	}

	void SetDamageText (int damage) {
		txt.text = damage.ToString();
	}

	IEnumerator AnimationCoroutine (int frame) {
		float max = 1;
		RectTransform rect = bg.GetComponent<RectTransform>();
		Vector2 before = rect.anchoredPosition;
		for(int i = 0; i <= frame; i++) {
			float value = Mathf.Lerp(max, 0, (float)i / (float)frame);
			canvasGroup.alpha = value;
			rect.anchoredPosition = before + new Vector2(0, (max - value)/max * 200);
			yield return null;	//go to next frame
		}
		Destroy(this.gameObject);
	}
}
