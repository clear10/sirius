﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainManager : SingletonMonoBehaviour<MainManager> {

	List<GameObject> party;
	Turn turn;
	bool isAsking;
	bool isRotate;
	TimeGauge gauge;
	int pixelWidth;
	int pixelHeight;

	MonsterInfo monsterInfo = null;
	Sprite monsterSprite = null;

	public Vector3 tileOrigin;
	public List<Hashtable> actLog = new List<Hashtable>();

	// Use this for initialization
	protected override void Awake () {
		base.Awake();
		party = new List<GameObject>();
		turn = new Turn();
	}

	void Start () {

		float screenYp = (GameManager.Instance.GetScreenSize()).y;
		pixelHeight = Mathf.RoundToInt(screenYp / (Camera.main.orthographicSize * 2f));
		pixelWidth = pixelHeight;

		//DebugText.Instance.Log(string.Format("pW={0}, pH={1}", pixelWidth, pixelHeight));

		//GameManager.Instance.SendMessage("OnLoadSceneMain", this, SendMessageOptions.DontRequireReceiver);
		GameManager.Instance.OnLoadSceneMain(this);
	}

	// Update is called once per frame
	void Update () {
		//if(Input.GetKeyDown(KeyCode.Space) && !isStartOnLoad) StartTurn();
	}
	public void StartTurn (GameObject obj) {
		//yield return new WaitForSeconds(t);
		Destroy(obj);
		StartTurn();
	}

	void StartTurn () {
		List<Unit> front = new List<Unit>();
		foreach(GameObject obj in party) {
			Unit unit = obj.GetComponent<Unit>();
			if(unit.isFrontLine()) front.Add(unit);
			unit.LoadRegParticle();
		}
		turn.CreateOrder(front);
		StartCoroutine("Step");
	}

	IEnumerator StartTurn (List<Unit> front, List<Unit> reg) {
		if(reg.Count > 0) {
			int count = 0;
			foreach(Unit u in reg) {
				if(u.GetRegType() == Unit.RegenerateType.Own) {
					u.Regenerate();
					count++;
				}
			}
			if(count > 0) yield return new WaitForSeconds(2f);
			foreach(Unit u in reg) {
				if(u.GetRegType() == Unit.RegenerateType.All) {
					string message = u.Regenerate();
					if(message == "Regenerated")
						yield return new WaitForSeconds(2f);
				}
			}
		}
		if(front.Count > 0) turn.CreateOrder(front);
		StartCoroutine("Step");
	}

	IEnumerator AskChangeRotation () {
		if(gauge == null) gauge = TimeGauge.Instance;
		DebugText.Instance.Log("AskChangeRotation");
		isAsking = true;
		SetRotateFlag(false);
		MainUIManager ui = GetComponent<MainUIManager>();
		ui.ShowAskingButtons(true);
		yield return StartCoroutine(gauge.CountDownCoroutine(3, 60));
		ui.ShowAskingButtons(false);
		isAsking = false;

		List<Unit> front = new List<Unit>();
		List<Unit> regenerater = new List<Unit>();
		foreach(GameObject obj in party) {
			Unit unit = obj.GetComponent<Unit>();
			bool ret = isRotate ? unit.Rotate() : unit.isFrontLine();
			if(ret) front.Add(unit);
			if(unit.isRegenerate()) regenerater.Add(unit);
		}
		// change
		yield return new WaitForSeconds(1f);
		StartCoroutine(StartTurn(front, regenerater));
		turn.Pass();
	}

	IEnumerator Step () {
		float delay = 1f;
		while(!turn.CheckFinTurn()) {
			yield return new WaitForSeconds(delay);
			turn.Update();
			while(!turn.CheckUpdateTurn()) { yield return 0; }
			//yield return 0;
		}
		yield return new WaitForSeconds(delay);
		StartCoroutine("AskChangeRotation");
	}

	public void AddToParty (GameObject obj) {
		party.Add(obj);
	}

	public void RemoveFromParty (GameObject obj) {
		party.Remove(obj);
	}

	public Unit GetUnitOnPosition (Vector2 tilePos) {
		if(tilePos.x > 3 || tilePos.y > 2) return null;
		foreach(GameObject obj in party) {
			Unit unit = obj.GetComponent<Unit>();
			if(tilePos == unit.GetTilePosition())
				if(obj.tag != "Enemy")
					return unit;
		}
		return null;
	}

	public List<Unit> GetUnitsOnLine (int y) {
		List<Unit> units = new List<Unit>();
		for(int i = 0; i < 4; i++) {
			Unit u = GetUnitOnPosition(new Vector2(i, y));
			if(u != null) units.Add(u);
		}

		return units;
	}

	public bool CheckExistRegenerateAll (Unit own, int y) {
		for(int i = 0; i < 4; i++) {
			Unit u = GetUnitOnPosition(new Vector2(i, y));
			if(u != null && u != own) {
				if(u.GetRegType() == Unit.RegenerateType.All) return true;
			}
		}
		return false;
	}

	public void FinishBattle (string winner) {
		DebugText.Instance.Log("Winner: " + winner);
		StopAllCoroutines();

		GameObject result = new GameObject("ResultInfo");
		BattleResult info = result.AddComponent<BattleResult>();
		Hashtable tbl = ArrangeLoggedData();
		MissionManager.Instance.BeatEnemy(tbl);
		if(!info.Save(tbl)) DebugText.Instance.Log("ActLogError");
		//UICanvas.Instance.AddDontDestroy(result);

		GoSceneReward();
	}

	Hashtable ArrangeLoggedData () {

		int myMaxDamage = 0;
		string myMaxDamageID = "";
		int yourMaxDamage = 0;
		string yourMaxDamageID = "";
		string lastID = "";
		int passedTurn = 0;
		bool isPlayerWin = false;
		bool isJustKill = false;
		string deadUnitsID = "";
		//List<UnitInfo> deadUnits = new List<UnitInfo>();

		int i = 0;
		int lastEnemyHp = 0;
		foreach(Hashtable hash in actLog) {
			string id = (string)hash["UnitID"];
			bool isPlayer = id.IndexOf("Unit") == -1 ? false : true;
			int d = (int)hash["Damage"];
			if(isPlayer && d > myMaxDamage) {
				myMaxDamage = d;
				myMaxDamageID = id;
			}
			else if(!isPlayer && d > yourMaxDamage) {
				yourMaxDamage = d;
				if(!hash.ContainsKey("TargetID")) DebugText.Instance.Log("HashKeyError");
				else yourMaxDamageID = hash["TargetID"] as string;
			}
			if(i == actLog.Count - 1) {
				Hashtable lastTable = actLog[i];
				lastID = lastTable["UnitID"] as string;
				passedTurn = (int)lastTable["Turn"];
				isPlayerWin = isPlayer;
				if(isPlayerWin && d == lastEnemyHp) isJustKill = true;
			}
			if(isPlayer) lastEnemyHp = (int)hash["TargetHP"];
			else {
				int unitHp = (int)hash["TargetHP"];
				if(unitHp == 0) {
					if(deadUnitsID == "") deadUnitsID = hash["TargetID"] as string;
					else deadUnitsID += "," + (string)hash["TargetID"];
				}
			}
			i++;
		}


		Hashtable resultHash = new Hashtable();
		resultHash.Add("passedTurn", passedTurn);
		resultHash.Add("myMaxDamage", myMaxDamage);
		resultHash.Add("myMaxDamageID", myMaxDamageID);
		resultHash.Add("yourMaxDamage", yourMaxDamage);
		resultHash.Add("yourMaxDamageID", yourMaxDamageID);
		resultHash.Add("isPlayerWin", isPlayerWin);
		resultHash.Add("strikerID", lastID);
		resultHash.Add("isJustKill", isJustKill);
		resultHash.Add("deadUnitsID", deadUnitsID);

		return resultHash;
	}

	public void SetRotateFlag (bool rat) {
		if(!isAsking) return;
		isRotate = rat;

		MainUIManager ui = GetComponent<MainUIManager>();
		ui.ChangeAskingButtonsColor(rat);
	}

	public void GoSceneReward () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Result");
	}

	public Vector2 TilePos2Pixel (int x, int y) {
		return new Vector2((x - 2) * pixelWidth, -y * pixelHeight);
	}

	public Vector2 PixelSize {
		get { return new Vector2(pixelWidth, pixelHeight); }
	}

	public Sprite MonsterSprite {
		get { return monsterSprite; }
	}

	public void LoggingActionData (Hashtable hash) {

		hash.Add("Turn", turn.Passed);
		actLog.Add(hash);
	}

	public void CopyMonsterInfo (MonsterInfo info) {
		this.monsterInfo = info;
		this.monsterSprite = (info == null) ? null : Resources.Load<Sprite>("Sprites/monster/" + info.Filename);
	}
}
