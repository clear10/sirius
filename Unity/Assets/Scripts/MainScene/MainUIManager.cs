﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainUIManager : UIController {

	GameObject btnWaitObj;
	GameObject btnRotateObj;

	Hashtable units = new Hashtable();

	protected override void RegistPrefabs (UICanvas canvas) {
		canvas.AddPrefab("CommonButton", "Prefabs/UI/CommonButton", true);
		canvas.AddPrefab("TimeGauge", "Prefabs/UI/TimeGauge");
		canvas.AddPrefab("HpGauge", "Prefabs/UI/HpGauge");
		canvas.AddPrefab("HpText", "Prefabs/UI/HpText");
		canvas.AddPrefab("HpGauge_Monster", "Prefabs/UI/HpGauge_Monster");
		canvas.AddPrefab("HpText_Monster", "Prefabs/UI/HpText_Monster");
		canvas.AddPrefab("CutInSpawner", "Prefabs/CutInSpawner");
		canvas.AddPrefab("ParticleRegenerateOwn", "Prefabs/ParticleRegenerateOwn");
		canvas.AddPrefab("AttackPanel", "Prefabs/UI/AttackPanel");
		canvas.AddPrefab("DamageEffect", "Prefabs/UI/DamageEffect");
	}

	protected override void ShowUI (UICanvas canvas, Transform parent) {
		GameObject timeGauge = Instantiate(canvas.GetPrefab("TimeGauge")) as GameObject;
		timeGauge.transform.SetParent(parent, false);
		GameObject cutin = Instantiate(canvas.GetPrefab("CutInSpawner")) as GameObject;
		cutin.name = "CutInSpawner";
		//cutin.transform.SetParent(MainManager.Instance.transform);

		btnWaitObj = Instantiate(canvas.GetPrefab("CommonButton")) as GameObject;
		btnRotateObj = Instantiate(canvas.GetPrefab("CommonButton")) as GameObject;
		MainManager manager = MainManager.Instance;
		InitButton(btnWaitObj, "Wait", parent, () => manager.SetRotateFlag(false));
		InitButton(btnRotateObj, "Rotate", parent, () => manager.SetRotateFlag(true));
		SetRectTransform(btnWaitObj, new Vector2(-200, 0), new Vector2(180, 100));
		SetRectTransform(btnRotateObj, new Vector2(200, 0), new Vector2(180, 100));
		ShowAskingButtons(false);

		GameObject btnStart = Instantiate(canvas.GetPrefab("CommonButton")) as GameObject;
		InitButton(btnStart, "Start", parent, () => manager.StartTurn(btnStart));
		SetRectTransform(btnStart, new Vector2(0, 640), new Vector2(150, 150));
	}

	public void ShowAskingButtons (bool flag) {
		btnWaitObj.SetActive(flag);
		btnRotateObj.SetActive(flag);
	}

	public void ChangeAskingButtonsColor (bool rat) {
		Image image = btnWaitObj.GetComponent<Image>();
		image.color = rat ? Color.white : Color.red;
		image = btnRotateObj.GetComponent<Image>();
		image.color = !rat ? Color.white : Color.red;
	}

	public void AddUnitUI (string unitID) {
		Hashtable hp = new Hashtable();
		string addition = unitID.Contains("Monster") ? "_Monster" : "";
		GameObject gauge = Instantiate(UICanvas.Instance.GetPrefab("HpGauge" + addition)) as GameObject;
		gauge.transform.SetParent(UICanvas.Instance.Base, false);
		GameObject text = Instantiate(UICanvas.Instance.GetPrefab("HpText" + addition)) as GameObject;
		text.transform.SetParent(UICanvas.Instance.Base, false);
		hp.Add("gauge", gauge);
		hp.Add("text", text);
		units.Add(unitID, hp);
		MainManager mm = MainManager.Instance;
		Vector2 size = mm.PixelSize;
		Vector2 textPos = Vector2.zero; //dummy
		if(addition == "") {
			SetRectTransform((GameObject)hp["text"], textPos, size);
			SetRectTransform((GameObject)hp["gauge"], new Vector2(textPos.x, textPos.y - size.y / 2f), new Vector2(size.x, size.x * 0.2f));
			return;
		}

		/**
		 * Monsterのスプライトは300x800で、300px=1Unityと設定してあるから
		 * Monsterの座標から400px=400/300Unityだけ下げている
		 **/

		Vector3 pos = new Vector3(1.5f, 2.5f, 0);
		pos.y = (pos.y - 4f / 3f) * size.y;
		pos.x = -300f;

		SetRectTransform((GameObject)hp["text"], pos, new Vector2(size.x * 0.5f, size.x * 8f / 3f));
		SetRectTransform((GameObject)hp["gauge"], pos, new Vector2(size.x * 0.5f, size.x * 8f / 3f));

	}

	public void UpdateUnitUI (string id, bool ret, Vector2 tilePos, int currentHp, int maxHp) {
		if(!units.ContainsKey(id)) return;

		Hashtable hp = units[id] as Hashtable;
		MainManager mm = MainManager.Instance;

		if(!id.Contains("Monster")) {
			Vector2 size = mm.PixelSize;
			Vector2 textPos = mm.TilePos2Pixel((int)tilePos.x, (int)tilePos.y);
			SetRectTransform((GameObject)hp["text"], textPos);
			SetRectTransform((GameObject)hp["gauge"], new Vector2(textPos.x, textPos.y - size.y / 2f));
		}

		if(ret) {
			((GameObject)hp["text"]).SetActive(ret);
			((GameObject)hp["gauge"]).SetActive(ret);
			UpdateUnitUI(id, currentHp, maxHp);
			return;
		}
		UpdateUnitUI(id, currentHp, maxHp);
		((GameObject)hp["text"]).SetActive(ret);
		((GameObject)hp["gauge"]).SetActive(ret);
		return;
	}

	public void UpdateUnitUI (string id, int currentHp, int maxHp) {

		Hashtable hp = units[id] as Hashtable;
		Text hpText = ((GameObject)hp["text"]).GetComponent<Text>();
		Image hpGauge = ((GameObject)hp["gauge"]).GetComponent<Image>();
		hpText.text = currentHp.ToString();
		hpGauge.fillAmount = (float)currentHp / (float)maxHp;
	}

	public void DestroyUnitUI (string id) {
		if(!units.ContainsKey(id)) {
			DebugText.Instance.Log("UnitUIerror: does not exist key->" + id);
			return;
		}

		Hashtable hp = units[id] as Hashtable;
		Text hpText = ((GameObject)hp["text"]).GetComponent<Text>();
		Image hpGauge = ((GameObject)hp["gauge"]).GetComponent<Image>();

		Destroy(hpText.gameObject);
		Destroy(hpGauge.gameObject);
		units.Remove(id);
	}

	public void ShowDamageEffect (int damage, Vector3 unitWorldPos) {
		GameObject damageEffect = Instantiate(UICanvas.Instance.GetPrefab("DamageEffect")) as GameObject;
		damageEffect.transform.SetParent(UICanvas.Instance.Base, false);
		Vector3 pos = unitWorldPos - Camera.main.transform.position;
		Vector2 pixelSize = MainManager.Instance.PixelSize;
		Vector2 canvasPos = new Vector2(pos.x * pixelSize.x, pos.y * pixelSize.y);
		SetRectTransform(damageEffect, canvasPos);
		DamageEffect effect = damageEffect.GetComponent<DamageEffect>();
		effect.StartAnimation(damage, 30);
	}
}