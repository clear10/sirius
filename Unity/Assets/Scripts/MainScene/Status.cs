﻿using UnityEngine;
using System.Collections;

public class Status {
	int hp;
	int atk;
	int agl;
	int s_atk;
	int s_def;
	int reg;

	int __maxHp;

	public bool isWriteable;

	public Status () {
		hp = 0;
		atk = 0;
		agl = 0;
		s_atk = 0;
		s_def = 0;
		reg = 0;
		isWriteable = false;
		__maxHp = hp;
	}
	
	public Status (int hp, int atk, int agl, int reg = 0) {
		this.hp = hp;
		this.atk = atk;
		this.agl = agl;
		this.s_atk = 0;
		this.s_def = 0;
		this.reg = reg;
		isWriteable = false;
		__maxHp = hp;
	}

	public Status (int hp, int atk, int agl, int s_atk, int s_def, int reg) {
		this.hp = hp;
		this.atk = atk;
		this.agl = agl;
		this.s_atk = s_atk;
		this.s_def = s_def;
		this.reg = reg;
		isWriteable = false;
		__maxHp = hp;
	}

	public Status (Status s) {
		this.hp = s.hp;
		this.atk = s.atk;
		this.agl = s.agl;
		this.s_atk = s.s_atk;
		this.s_def = s.s_def;
		this.reg = s.reg;
		this.isWriteable = s.isWriteable;
		__maxHp = hp;
	}

	public int Damage (int damage) {
		isWriteable = true;
		Hp -= damage;
		if(Hp < 0) Hp = 0;

		isWriteable = false;
		return Hp;
	}

	public int Regenerate (int value) {
		isWriteable = true;
		Hp += value;
		if(Hp > __maxHp) Hp = __maxHp;

		isWriteable = false;
		return Hp;
	}

	public int Hp {
		get { return hp; }
		set {
			if(isWriteable) hp = value;
		}
	}

	public int Atk {
		get { return atk; }
		set { if(isWriteable) atk = value; }
	}

	public int Agl {
		get { return agl; }
		set { if(isWriteable) agl = value; }
	}

	public int SAtk {
		get { return s_atk; }
		set { if(isWriteable) s_atk = value; }
	}

	public int SDef {
		get { return s_def; }
		set { if(isWriteable) s_def = value; }
	}

	public int Reg {
		get { return reg; }
		set { if(isWriteable) reg = value; }
	}

	public int MaxHp {
		get { return __maxHp; }
	}
}
