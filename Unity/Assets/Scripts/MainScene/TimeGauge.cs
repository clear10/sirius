﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeGauge : SingletonMonoBehaviour<TimeGauge> {

	public Text text;
	public Image gauge;

	// Use this for initialization
	void Start () {
		//gauge = GetComponent<Image>();
		//if(gauge == null) DebugText.Instance.LogError("Gauge did not find.");
		text.gameObject.SetActive(false);
		gauge.gameObject.SetActive(false);
		
		gauge.fillAmount = 0;
		//this.StartCount(3f);

	}

	public void StartCount (float sec) {
		text.text = sec.ToString();
		text.gameObject.SetActive(true);
		gauge.gameObject.SetActive(true);
		StartCoroutine(CountDownCoroutine((int)sec, 60));
	}

	public IEnumerator CountDownCoroutine (int sec, int frame) {
		float interval = 1 / (float)frame;
		int n = sec;
		text.text = sec.ToString();
		text.gameObject.SetActive(true);
		gauge.gameObject.SetActive(true);
		for(int j = 0; j < n; j++) {
			for(int i = 0; i < frame; i++) {
				yield return new WaitForSeconds(interval);
				if(gauge.fillOrigin == 2) gauge.fillAmount += interval;
				else gauge.fillAmount -= interval;
			}

			gauge.fillOrigin = 2 - gauge.fillOrigin;
			gauge.rectTransform.Rotate(0, 0, 180);
			gauge.fillClockwise = !gauge.fillClockwise;

			sec -= 1;
			text.text = sec.ToString();
		}
		text.gameObject.SetActive(false);
		gauge.gameObject.SetActive(false);
	}
}
