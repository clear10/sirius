﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Turn {

	List<Unit> order;
	int count;		//現在のターンのステップ数

	int current;	//経過ターン数

	public int Passed { get { return current; } }

	public Turn () {
		order = new List<Unit>();
		count = 0;
		current = 1;
	}

	public void CreateOrder (List<Unit> front) {
		order.Clear();
		front.Sort((a, b) => b.Agility - a.Agility);
		foreach(Unit u in front) {
			order.Add(u);
		}
		count = 0;
	}

	public void Update () {
		if(count >= order.Count) return;
		if(order[count] == null) return;
		order[count].Act();
	}

	public bool CheckFinTurn () {
		if(count < order.Count)
			if(order[count] == null) return true;
		return count >= order.Count ? true : false;
	}

	public bool CheckUpdateTurn () {
		Unit unit = order[count];
		if(unit == null) return true;
		if(!unit.CheckFinAct()) return false;

		count++;
		return true;
	}

	public int Pass () {
		return ++current;
	}
}
