﻿using UnityEngine;
using System.Collections;

public class Archer : Unit {

	const string jobString = "Archer";

	// ゴリゴリと書いていく予定
	public override void Awake () {
		base.Awake();
		type_ = RegenerateType.None;
	}

	public override IEnumerator LoadResources () {
		string cutinKey = "Cutin" + jobString;
		UICanvas.Instance.AddPrefab(cutinKey, "Prefabs/myp_archer");
		cutinPrefab = UICanvas.Instance.GetPrefab(cutinKey);
		if(gameObject.GetComponent<SpriteRenderer>() == null) {
			string spriteKey = "Sprite" + jobString;
			UICanvas.Instance.AddPrefab(spriteKey, "Prefabs/icon_archer");
			GameObject sprite = UICanvas.Instance.GetPrefab(spriteKey);
			sprite = (GameObject)Instantiate(sprite);
			sprite.transform.parent = this.transform;
			sprite.transform.localPosition = Vector3.zero;
		}
		yield return null;
		isPrepared = true;
	}

	public override void Init (string name, Vector2 tilePos, Vector3 tileOrigin) {
		//Debug.Log("knight init");
		StartCoroutine(LoadResources());
		base.Init(name, (string)jobString, tilePos, tileOrigin);
	}

	protected override IEnumerator ActDirection (Unit target, GameObject cutin, int damage, bool isDestroyTarget) {
		yield return StartCoroutine(base.ActDirection(target, cutin, damage, isDestroyTarget));

		if(isDestroyTarget) StartCoroutine(DestroyOnNextFrame(target.gameObject));
	}

	public override bool isFrontLine () {
		/**
		 * アーチャーは前列では攻撃しない
		 * 中列でのみ攻撃する
		 * そのため中列にいるときだけ行動順に加える
		 **/
		return (GetTilePosition().y == 1);
	}
}
