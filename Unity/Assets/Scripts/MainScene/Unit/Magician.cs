﻿using UnityEngine;
using System.Collections;

public class Magician : Unit {

	const string jobString = "Magician";

	// ゴリゴリと書いていく予定
	public override void Awake () {
		base.Awake();
		type_ = RegenerateType.None;
	}

	public override IEnumerator LoadResources () {
		string cutinKey = "Cutin" + jobString;
		UICanvas.Instance.AddPrefab(cutinKey, "Prefabs/myp_magician");
		cutinPrefab = UICanvas.Instance.GetPrefab(cutinKey);
		if(gameObject.GetComponent<SpriteRenderer>() == null) {
			string spriteKey = "Sprite" + jobString;
			UICanvas.Instance.AddPrefab(spriteKey, "Prefabs/icon_magician");
			GameObject sprite = UICanvas.Instance.GetPrefab(spriteKey);
			sprite = (GameObject)Instantiate(sprite);
			sprite.transform.parent = this.transform;
			sprite.transform.localPosition = Vector3.zero;
		}
		yield return null;
		isPrepared = true;
	}

	public override void Init (string name, Vector2 tilePos, Vector3 tileOrigin) {
		//Debug.Log("knight init");
		StartCoroutine(LoadResources());
		base.Init(name, (string)jobString, tilePos, tileOrigin);
	}

	protected override IEnumerator ActDirection (Unit target, GameObject cutin, int damage, bool isDestroyTarget) {
		yield return StartCoroutine(base.ActDirection(target, cutin, damage, isDestroyTarget));

		if(isDestroyTarget) StartCoroutine(DestroyOnNextFrame(target.gameObject));
	}

	public override bool isFrontLine () {
		/**
		 * マジシャンは戦闘に出ている限り毎ターンどこでも攻撃できる
		 * 実装的に常に前列にいることにしたほうが楽なのでマジシャンだけオーバーライドする
		 **/
		return true;
	}
}
