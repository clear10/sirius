﻿using UnityEngine;
using System.Collections;

public class Monster : Unit {

	const string jobString = "Monster";

	// ゴリゴリと書いていく予定
	public override void Awake () {
		base.Awake();
		//Debug.Log("Knight");
		type_ = RegenerateType.None;
	}

	public override IEnumerator LoadResources () {
		if(gameObject.GetComponent<SpriteRenderer>() == null) {
			string spriteKey = "Sprite" + jobString;
			UICanvas.Instance.AddPrefab(spriteKey, "Prefabs/icon_monster");
			GameObject sprite = UICanvas.Instance.GetPrefab(spriteKey);
			sprite = (GameObject)Instantiate(sprite);
			sprite.transform.parent = this.transform;
			sprite.transform.localPosition = Vector3.zero;

			SpriteRenderer sr = sprite.GetComponent<SpriteRenderer>();
			Sprite monster = MainManager.Instance.MonsterSprite;
			try {
				sr.sprite = monster;
			}
			catch(System.Exception e) {
				DebugText.Instance.Log(e.Message);
			}

		}
		yield return null;
		isPrepared = true;
	}

	public override void Init (string name, Vector2 tilePos, Vector3 tileOrigin) {
		//Debug.Log("knight init");
		isPlayer = false;

		//	モンスターのみの特別処理
		type_ = (this.param.Reg > 0) ? RegenerateType.Own : RegenerateType.None;

		StartCoroutine(LoadResources());
		base.Init(name, (string)jobString, tilePos, tileOrigin);
		StartCoroutine(PositionUpdate(tileOrigin, 1));
	}

	protected override IEnumerator ActDirection (Unit target, GameObject cutin, int damage, bool isDestroyTarget) {
		yield return StartCoroutine(base.ActDirection(target, cutin, damage, isDestroyTarget));

		if(isDestroyTarget) StartCoroutine(DestroyOnNextFrame(target.gameObject));
	}

	protected override IEnumerator RegenerateDirection () {
		if(type_ != RegenerateType.Own) yield break;

		int hp = param.Regenerate(param.Reg);
		DebugText.Instance.Log(name_ + ": " + hp.ToString());
		yield return "Regenerated";
	}
}
