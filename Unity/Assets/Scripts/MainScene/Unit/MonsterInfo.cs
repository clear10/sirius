﻿using UnityEngine;
using System.Collections;

public class MonsterInfo {

	int id;
	string name;
	int appearanceTime;
	string beforeBattleLabel;
	string afterBattleLabel;
	string filename;
	bool regenerate;
	Status param;

	public MonsterInfo (int id, string name, int appTime, string beforeLabel, string afterLabel, string filename, bool reg, Status status) {
		this.id = id;
		this.name = name;
		this.appearanceTime = appTime;
		this.beforeBattleLabel = beforeLabel;
		this.afterBattleLabel = afterLabel;
		this.filename = filename;
		this.regenerate = reg;
		this.param = new Status(status);
	}

	public int ID { get { return id; } }
	public string Name { get { return name; } }
	public int Appearance { get { return appearanceTime; } }
	public string BeforeBattleLabel { get { return beforeBattleLabel; } }
	public string AfterBattleLabel { get { return afterBattleLabel; } }
	public string Filename { get { return filename; } }
	public bool Regenerate { get { return regenerate; } }
	public Status Param { get { return param; } }
}
