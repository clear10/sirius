﻿using UnityEngine;
using System.Collections;

public class Thief : Unit {

	const string jobString = "Thief";

	// ゴリゴリと書いていく予定
	public override void Awake () {
		base.Awake();
		//DebugText.Instance.Log("Knight");
		type_ = RegenerateType.Own;
	}

	public override IEnumerator LoadResources () {
		string cutinKey = "Cutin" + jobString;
		UICanvas.Instance.AddPrefab(cutinKey, "Prefabs/myp_thief");
		cutinPrefab = UICanvas.Instance.GetPrefab(cutinKey);
		if(gameObject.GetComponent<SpriteRenderer>() == null) {
			string spriteKey = "Sprite" + jobString;
			UICanvas.Instance.AddPrefab(spriteKey, "Prefabs/icon_thief");
			GameObject sprite = UICanvas.Instance.GetPrefab(spriteKey);
			sprite = (GameObject)Instantiate(sprite);
			sprite.transform.parent = this.transform;
			sprite.transform.localPosition = Vector3.zero;
		}
		yield return null;
		isPrepared = true;
	}

	public override void Init (string name, Vector2 tilePos, Vector3 tileOrigin) {
		//DebugText.Instance.Log("knight init");
		StartCoroutine(LoadResources());
		base.Init(name, (string)jobString, tilePos, tileOrigin);
	}

	protected override IEnumerator ActDirection (Unit target, GameObject cutin, int damage, bool isDestroyTarget) {
		yield return StartCoroutine(base.ActDirection(target, cutin, damage, isDestroyTarget));

		if(isDestroyTarget) StartCoroutine(DestroyOnNextFrame(target.gameObject));
	}

	protected override IEnumerator RegenerateDirection () {
		if(type_ != RegenerateType.Own) yield break;

		ParticleSystem ps = regParticle.GetComponent<ParticleSystem>();
		ps.Play();

		int hp = param.Regenerate(param.Reg);
		DebugText.Instance.Log(name_ + ": " + hp.ToString());
		yield return "Regenerated";
	}
}
