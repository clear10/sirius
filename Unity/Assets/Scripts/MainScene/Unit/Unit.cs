﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Unit : MonoBehaviour {

	public enum RegenerateType {
		None,
		Own,
		All,
	};

	Vector3 tileOrigin;
	Vector2 tilePos;
	bool isAwaked = false;
	GameObject attackPanel = null;
	string id = "";
	int supportAtkCount = 0;
	int supportDefCount = 0;

	protected Status param;
	protected bool isPlayer = true;
	protected RegenerateType type_;
	protected GameObject cutinPrefab;
	protected GameObject regParticle;

	public bool isActing;
	public bool isPrepared;
	public string name_;

	public int Agility {
		get { return param.Agl; }
	}

	public virtual IEnumerator LoadResources () {
		yield return null;
	}

	// Use this for initialization
	public virtual void Awake () {
		if(isAwaked) return;

		isPlayer = true;
		param = new Status();
		tilePos = new Vector2();
		tileOrigin = new Vector3();
		type_ = RegenerateType.None;
		name_ = "Dummy";
		isActing = false;
		isPrepared = false;
		isAwaked = true;
		//regParticle = (GameObject)Resources.Load("Prefabs/ParticleRegenerateOwn");
		//DebugText.Instance.Log("Awake" + isAwaked.ToString());
	}

	// Update is called once per frame
	void Update () {

	}

	public virtual void Init (string name, Vector2 tilePos, Vector3 tileOrigin) { }

	public void Init (string name, string jobString, Vector2 tilePos, Vector3 tileOrigin) {
		//DebugText.Instance.Log("Init" + name);
		this.name_ = name;
		SetPosition(tilePos);
		this.tileOrigin = tileOrigin;
		gameObject.name = jobString != "" ? name + "_" + jobString : name + "_Dummy";
		gameObject.tag = isPlayer ? "Player" : "Enemy";

		//if(isPlayer) {
		MainUIManager ui = MainManager.Instance.gameObject.GetComponent<MainUIManager>();
		ui.AddUnitUI(this.id);
		//}
		StartCoroutine("PositionUpdate", 1);
	}

	public void PublishID (string id) {
		this.id = id;
	}

	IEnumerator PositionUpdate (int frame) {
		if(!isPlayer) yield break;
		Vector3 from = transform.position;
		Vector3 to = new Vector3(tileOrigin.x + tilePos.x, -(tileOrigin.y + tilePos.y), 0);
		if(Application.loadedLevelName == "Main") {
			MainManager mm = MainManager.Instance;
			UpdateUI(false, mm);
			for(int i = 1; i <= frame; i++) {
				transform.position = Vector3.Lerp(from, to, (float)i / (float)frame);
				yield return 0;
			}
			UpdateUI(true, mm);
			yield break;
		}
		for(int i = 1; i <= frame; i++) {
			transform.position = Vector3.Lerp(from, to, (float)i / (float)frame);
			yield return 0;
		}
	}

	protected IEnumerator PositionUpdate (Vector3 pos, int frame) {
		//if(!isPlayer) yield break;
		Vector3 from = transform.position;
		Vector3 to = pos;
		MainManager mm = MainManager.Instance;
		UpdateUI(false, mm);
		for(int i = 1; i <= frame; i++) {
			transform.position = Vector3.Lerp(from, to, (float)i / (float)frame);
			yield return 0;
		}
		UpdateUI(true, mm);
	}

	protected IEnumerator WaitForFrames (int n) {
		for(int i = 0; i < n; i++) {
			yield return 0;
		}
	}

	public void Act () {
		DebugText.Instance.Log("Act(" + name_ + ")");
		isActing = true;
		MainManager mm = MainManager.Instance;

		float damageRate = 1.0f;

		Unit target = null;
		if(isPlayer) {
			GameObject obj = GameObject.FindWithTag("Enemy");
			if(obj != null) target = obj.GetComponent<Unit>();
		}
		else {
			List<GameObject> objs = new List<GameObject>();	//GameObject.FindGameObjectsWithTag("FrontPlayer");
			int i = 0;
			while(objs.Count == 0) {
				List<Unit> units = mm.GetUnitsOnLine(i);
				if(units.Count == 0) {
					i++;
					//	ダメージ倍率上昇
					damageRate += 0.5f;
					continue;
				}

				foreach(Unit u in units) objs.Add(u.gameObject);
				i++;
			}

			float r = Random.Range(-0.5f, (float)(objs.Count - 1) + 0.5f);
			int j = Mathf.RoundToInt(r);
			// あまりにも偏るのでちょっと補正
			i = (Mathf.Sin(Time.time) > 0) ? j : objs.Count - 1 - j;
			target = objs[i].GetComponent<Unit>();
		}

		if(target == null) {
			isActing = false;
			return;
		}

		Unit back = null;
		int supportAttack = 0;
		int supportDefence = 0;
		if(this.isPlayer) {
			/**
			 * サポートを受けるのは前列のみ
			 * マジシャンコンポーネントを持っている = マジシャン
			 * マジシャンだけはサポートアタックを受け付けない(仕様)
			 **/
			if(this.GetTilePosition().y == 0 && this.GetComponent<Magician>() == null) {
				back = mm.GetUnitOnPosition(new Vector2(tilePos.x, 1));
				if(back != null) supportAttack = (back.supportAtkCount <= 0) ? back.param.SAtk : 0;
				supportDefence = 0;
			}
		}
		else {
			if(target.GetTilePosition().y == 0) {
				back = mm.GetUnitOnPosition(new Vector2(target.tilePos.x, 1));
				supportAttack = 0;
				if(back != null) supportDefence = (back.supportDefCount <= 0) ? back.param.SDef : 0;
			}
		}
		//	ダメージ計算式
		int d = (int)((float)this.param.Atk * damageRate + (float)supportAttack - (float)supportDefence);
		//	ダメージを与える処理
		int hp = target.param.Damage(d);
		if(supportAttack > 0) {
			DebugText.Instance.Log(back.name_ + " supported(Attack)");
			back.supportAtkCount++;
		}
		else if(supportDefence > 0) {
			DebugText.Instance.Log(back.name_ + " supported(Defence)");
			back.supportDefCount++;
		}
		else {
			back = null;
		}
		DebugText.Instance.Log("Last: " + hp.ToString() + "(" + target.name_ + ")");
		bool ret = (hp == 0);
		if(ret) mm.RemoveFromParty(target.gameObject);

		GameObject ci = (back == null) ? null : back.GetCutIn();

		Hashtable data = new Hashtable();
		data.Add("UnitID", this.id);
		data.Add("Damage", d);
		data.Add("TileX", (int)tilePos.x);
		data.Add("TileY", (int)tilePos.y);
		data.Add("TargetHP", hp);
		if(!isPlayer) data.Add("TargetID", target.GetUnitID());
		mm.LoggingActionData(data);

		StartCoroutine(ActDirection(target, ci, d, ret));
	}

	protected virtual IEnumerator ActDirection (Unit target, GameObject cutin, int damage, bool isDestroyTarget) {
		int frame1 = 10;
		int frame2 = 20;

		Vector3 from = this.gameObject.transform.position;
		Vector3 to = target.gameObject.transform.position;
		CutIn ci = null;
		if(cutin != null) {
			ci = CutIn.Instance;
			float x = (target.isPlayer) ? to.x : from.x;
			CutIn.Side lr = (x < 1.5f) ? CutIn.Side.Right : CutIn.Side.Left;
			CutIn.Side ud = target.isPlayer ? CutIn.Side.Up : CutIn.Side.Down;

			ci.StartCutIn(cutin, ud, lr, (frame1 + frame2) * 2);
		}
		if(!target.isPlayer) {
			if(attackPanel == null) attackPanel = UICanvas.Instance.GetPrefab("AttackPanel");
			if(ci == null) ci = CutIn.Instance;
			float x = from.x;
			CutIn.Side lr = (x < 1.5f) ? CutIn.Side.Right : CutIn.Side.Left;

			attackPanel = Instantiate(attackPanel) as GameObject;

			AttackPanel pnl = attackPanel.GetComponent<AttackPanel>();
			SpriteRenderer rend = transform.GetChild(0).GetComponent<SpriteRenderer>();
			pnl.SetPanel(rend.sprite, name_, damage);

			AnimateSpriteScale(true, false);
			ci.StartCutIn(attackPanel, lr, CutIn.Side.Up, (frame1 + frame2) * 2, true);
			yield return StartCoroutine(WaitForFrames((frame1 + frame2)));

			target.UpdateUI();
			EffectDamage(damage, target);

			yield return StartCoroutine(WaitForFrames((frame1 + frame2) - 30));
			AnimateSpriteScale(false, true);
			yield return StartCoroutine(WaitForFrames(30));
		}
		else {
			StartCoroutine(PositionUpdate(to, frame1));
			yield return StartCoroutine(WaitForFrames(frame1));

			target.UpdateUI();
			EffectDamage(damage, target);

			StartCoroutine(PositionUpdate(from, frame2));
			yield return StartCoroutine(WaitForFrames(frame2));
		}

		if(cutin != null) yield return StartCoroutine(WaitForFrames((frame1 + frame2)));
		attackPanel = null;
		isActing = false;
	}

	void AnimateSpriteScale (bool big, bool small) {
		if(big == small) return;
		Animator animator = transform.GetChild(0).GetComponent<Animator>();
		if(animator == null) return;

		if(big) {
			animator.SetTrigger("TriggerBig");
			transform.Translate(Vector3.back);
			return;
		}
		if(small) {
			animator.SetTrigger("TriggerSmall");
			transform.Translate(Vector3.forward);
			return;
		}
	}

	void EffectDamage (int damage, Unit target) {
		MainUIManager ui = MainManager.Instance.GetComponent<MainUIManager>();
		if(ui == null) return;
		Vector3 offset = Vector3.zero;
		if(!target.isPlayer)
			offset = (this.tilePos.x < 2) ? Vector3.left : Vector3.right;
		ui.ShowDamageEffect(damage, target.gameObject.transform.position + offset);
	}

	public void UpdateUI (bool ret, MainManager mm) {
		MainUIManager ui = mm.gameObject.GetComponent<MainUIManager>();
		if(ui == null) return;

		ui.UpdateUnitUI(this.id, ret, GetTilePosition(), param.Hp, param.MaxHp);
	}

	void UpdateUI () {
		MainUIManager ui = MainManager.Instance.gameObject.GetComponent<MainUIManager>();
		if(ui == null) return;

		ui.UpdateUnitUI(this.id, param.Hp, param.MaxHp);
	}

	protected IEnumerator DestroyOnNextFrame (GameObject target) {
		int last = 0;
		string winner = "Enemy";
		if(!isPlayer) last = GameObject.FindGameObjectsWithTag("Player").Length;	// + GameObject.FindGameObjectsWithTag("FrontPlayer").Length;
		else {
			last = GameObject.FindGameObjectsWithTag("Enemy").Length;
			winner = "Player";
		}

		MainUIManager ui = MainManager.Instance.GetComponent<MainUIManager>();
		ui.DestroyUnitUI(target.GetComponent<Unit>().GetUnitID());

		if(last == 1) MainManager.Instance.FinishBattle(winner);
		yield return 0;
		Destroy(target);
	}

	public string Regenerate () {
		var coroutine = RegenerateDirection();
		StartCoroutine(coroutine);
		string message = coroutine.Current as string;
		this.UpdateUI();

		return message;
	}

	public int Regenerate (int value, RegenerateType type) {
		if(type == RegenerateType.All) {
			//Accept
			ParticleSystem ps = regParticle.GetComponent<ParticleSystem>();
			ps.Play();
			int hp = this.param.Regenerate(value);
			this.UpdateUI();
			return hp;
		}
		return -1;
	}

	protected virtual IEnumerator RegenerateDirection () {
		Debug.LogError("Unit Regenerate called");
		yield return 0;
	}

	public bool CheckFinAct () {
		return !isActing;
	}

	public virtual bool isFrontLine () {
		return (tilePos.y == 0);
	}

	public bool isRegenerate () {
		if(type_ == RegenerateType.None) return false;
		if(!isPlayer) return true;
		return (tilePos.y == 2);
	}

	public void LoadRegParticle () {
		if(regParticle != null) return;
		if(type_ != RegenerateType.Own) {
			MainManager mm = MainManager.Instance;
			bool ret = mm.CheckExistRegenerateAll(this, (int)tilePos.y);
			if(!ret) return;
		}
		regParticle = UICanvas.Instance.GetPrefab("ParticleRegenerateOwn");
		Vector3 locPos = regParticle.transform.localPosition;
		regParticle = (GameObject)Instantiate(regParticle, transform.position, regParticle.transform.rotation);
		regParticle.transform.SetParent(this.transform, false);
		regParticle.transform.localPosition = locPos;
	}

	public bool Rotate () {
		if(!isPlayer) return true;
		SetPosition(new Vector2(tilePos.x, (tilePos.y + 2) % 3));
		supportAtkCount = 0;
		supportDefCount = 0;
		StartCoroutine("PositionUpdate", 30);
		return isFrontLine();
	}

	public Vector2 GetTilePosition () {
		return tilePos;
	}

	public RegenerateType GetRegType () {
		return this.type_;
	}

	public GameObject GetCutIn () {
		return cutinPrefab;
	}

	public string GetUnitID () {
		return id;
	}

	public void SetPosition (Vector2 pos) {
		tilePos = pos;
		//gameObject.tag = isPlayer ? "Player" : "Enemy";
	}

	public void SetParam (Status s) {
		param = new Status(s);
	}

	void OnDestroy () {
		StopAllCoroutines();
	}
}
