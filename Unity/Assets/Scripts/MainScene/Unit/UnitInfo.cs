﻿using UnityEngine;
using System.Collections;

public class UnitInfo {

	Vector2 tilePos;
	Status param;
	public JobClass job;
	string name;
	string id;

	#region ジョブを追加するときに編集しなければいけない箇所
	public enum JobClass {
		None,
		Knight,
		Fighter,
		Wizard,
		Thief,
		Priest,
		Magician,
		Archer,
		Monster,
	};

	JobClass ConvertString2Job (string s) {
		switch(s) {
			case "Knight":
				return JobClass.Knight;
			case "Wizard":
				return JobClass.Wizard;
			case "Fighter":
				return JobClass.Fighter;
			case "Thief":
				return JobClass.Thief;
			case "Priest":
				return JobClass.Priest;
			case "Magician":
				return JobClass.Magician;
			case "Archer":
				return JobClass.Archer;
			default:
				DebugText.Instance.Log("This is NEET(" + s + ")");
				return JobClass.None;
		}
	}

	public string GetUnitInfoString () {
		string s = this.id + ",";
		switch(job) {
			case JobClass.Knight:
				s += "Knight";
				break;
			case JobClass.Fighter:
				s += "Fighter";
				break;
			case JobClass.Wizard:
				s += "Wizard";
				break;
			case JobClass.Thief:
				s += "Thief";
				break;
			case JobClass.Priest:
				s += "Priest";
				break;
			case JobClass.Magician:
				s += "Magician";
				break;
			case JobClass.Archer:
				s += "Archer";
				break;
			default:
				return null;
		}
		s += "," + this.name;
		s += "," + this.param.Hp.ToString("00");
		s += "," + this.param.Atk.ToString("00");
		s += "," + this.param.Agl.ToString("00");
		s += "," + this.param.SAtk.ToString("00");
		s += "," + this.param.SDef.ToString("00");
		s += "," + this.param.Reg.ToString("00");
		s += "," + this.tilePos.x.ToString("00");
		s += "," + this.tilePos.y.ToString("00");

		//DebugText.Instance.Log(s);
		return s;
	}

	public string GetSpritePath () {
		switch(job) {
			case UnitInfo.JobClass.Knight:
				return "Sprites/icon/f017";
			case UnitInfo.JobClass.Fighter:
				return "Sprites/icon/f021";
			case UnitInfo.JobClass.Wizard:
				return "Sprites/icon/f019";
			case UnitInfo.JobClass.Thief:
				return "Sprites/icon/f011";
			case UnitInfo.JobClass.Priest:
				return "Sprites/icon/f073";
			case JobClass.Magician:
				return "Sprites/icon/f103";
			case JobClass.Archer:
				return "Sprites/icon/f132";
			case UnitInfo.JobClass.Monster:
				return "Sprites/monster/f010";
			default:
				return null;
		}
	}

	public Unit.RegenerateType GetRegType () {
		switch(job) {
			case UnitInfo.JobClass.Knight:
				return Unit.RegenerateType.Own;
			case UnitInfo.JobClass.Fighter:
				return Unit.RegenerateType.None;
			case UnitInfo.JobClass.Wizard:
				return Unit.RegenerateType.All;
			case UnitInfo.JobClass.Thief:
				return Unit.RegenerateType.Own;
			case UnitInfo.JobClass.Priest:
				return Unit.RegenerateType.None;
			case JobClass.Magician:
				return Unit.RegenerateType.None;
			case JobClass.Archer:
				return Unit.RegenerateType.None;
			case UnitInfo.JobClass.Monster:
				return Unit.RegenerateType.None;
			default:
				return Unit.RegenerateType.None;
		}
	}

	public Unit AddJobComponent (GameObject obj) {
		if(obj == null) return null;
		switch(job) {
			case UnitInfo.JobClass.Knight:
				return obj.AddComponent<Knight>();
			case UnitInfo.JobClass.Fighter:
				return obj.AddComponent<Fighter>();
			case UnitInfo.JobClass.Wizard:
				return obj.AddComponent<Wizard>();
			case UnitInfo.JobClass.Thief:
				return obj.AddComponent<Thief>();
			case UnitInfo.JobClass.Priest:
				return obj.AddComponent<Priest>();
			case JobClass.Magician:
				return obj.AddComponent<Magician>();
			case JobClass.Archer:
				return obj.AddComponent<Archer>();
			case UnitInfo.JobClass.Monster:
				return obj.AddComponent<Monster>();
			default:
				return null;
		}
	}
	#endregion

	public UnitInfo () { }
	public UnitInfo (string id, string name, string jobStr, Vector2 tilePos, Status param) {
		this.id = id;
		this.name = name;
		this.job = ConvertString2Job(jobStr);
		this.tilePos = tilePos;
		this.param = param;
	}

	public UnitInfo (MonsterInfo info) {
		this.id = "Monster" + info.ID.ToString("00");
		this.name = info.Name;
		this.job = JobClass.Monster;
		this.tilePos = Vector2.zero;
		this.param = info.Param;
	}

	public Vector2 GetTilePosition () {
		return tilePos;
	}

	public JobClass GetJobClass () {
		return job;
	}

	public Status GetParam () {
		return param;
	}

	public string GetID () {
		return id;
	}

	public string GetName () {
		return name;
	}

	public void SetTilePosition (Vector2 value) {
		tilePos = value;
	}
}
