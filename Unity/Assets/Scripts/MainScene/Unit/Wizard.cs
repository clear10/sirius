﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wizard : Unit {

	const string jobString = "Wizard";
	MainManager mm;

	// ゴリゴリと書いていく予定
	public override void Awake () {
		base.Awake();
		//Debug.Log("Knight");
		type_ = RegenerateType.All;
	}

	public override IEnumerator LoadResources () {
		string cutinKey = "Cutin" + jobString;
		UICanvas.Instance.AddPrefab(cutinKey, "Prefabs/myp_wizard");
		cutinPrefab = UICanvas.Instance.GetPrefab(cutinKey);
		if(gameObject.GetComponent<SpriteRenderer>() == null) {
			string spriteKey = "Sprite" + jobString;
			UICanvas.Instance.AddPrefab(spriteKey, "Prefabs/icon_wizard");
			GameObject sprite = UICanvas.Instance.GetPrefab(spriteKey);
			sprite = (GameObject)Instantiate(sprite);
			sprite.transform.parent = this.transform;
			sprite.transform.localPosition = Vector3.zero;
		}
		yield return null;
		isPrepared = true;
	}

	public override void Init (string name, Vector2 tilePos, Vector3 tileOrigin) {
		//Debug.Log("knight init");
		StartCoroutine(LoadResources());
		base.Init(name, (string)jobString, tilePos, tileOrigin);
	}

	protected override IEnumerator ActDirection (Unit target, GameObject cutin, int damage, bool isDestroyTarget) {
		yield return StartCoroutine(base.ActDirection(target, cutin, damage, isDestroyTarget));

		if(isDestroyTarget) StartCoroutine(DestroyOnNextFrame(target.gameObject));
	}

	protected override IEnumerator RegenerateDirection () {
		if(type_ != RegenerateType.All) yield break;

		if(mm == null) mm = MainManager.Instance;
		int y = (int)this.GetTilePosition().y;
		List<Unit> units = mm.GetUnitsOnLine(y);
		bool flag = false;
		foreach(Unit u in units) {
			if(u.gameObject.name != this.gameObject.name) {
				int hp = u.Regenerate(this.param.Reg, this.type_);
				DebugText.Instance.Log(u.name_ + ": " + hp.ToString() + " by Wizard");
				flag = true;
			}
		}

		yield return flag ? "Regenerated" : "None";
	}
}
