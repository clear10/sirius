﻿using UnityEngine;
using System.Collections;

public class Mission {

	public enum Type {
		Organize = 1,
		Battle,
		Request,
		Other,

		// Add here
	};

	public enum State {
		NOT_RECEIVED = 0,
		CHALLENGING,
		ACHIEVED,

		UNKNOWN = -1,
	};

	public class Builder {
		#region essential params
		public int id;
		public string title;
		public string summary;
		public Type type;
		#endregion

		#region optional params
		public int awardType;
		public int awardId;

		public string subject;
		public string comparion;
		public int value;

		public int appearanceTime;
		public int beforeMissionId;
		#endregion

		public Builder (int id, string title, string summary, Type type) {
			this.id = id;
			this.title = title;
			this.summary = summary;
			this.type = type;
		}

		public Builder award (int _type, int _id) {
			awardType = _type;
			awardId = _id;
			return this;
		}

		public Builder detail (string subject, string comparion, int value) {
			this.subject = subject;
			this.comparion = comparion;
			this.value = value;
			return this;
		}

		public Builder condition (int appearance, int missionId) {
			this.appearanceTime = appearance;
			this.beforeMissionId = missionId;
			return this;
		}

		public Data build () {
			return new Data(this);
		}

	}

	public class Data {
		/** 報酬に関する情報 */
		public class Award {
			public readonly int type;
			public readonly int id;

			public Award (int type, int id) {
				this.type = type;
				this.id = id;
			}
		}
		/** 判定詳細 */
		public class Detail {
			public readonly string subject;
			public readonly string comparion;
			public readonly int value;

			public Detail (string subject, string comp, int value) {
				this.subject = subject;
				this.comparion = comp;
				this.value = value;
			}
		}
		/** 出現条件に関する情報 */
		public class Condition {
			public readonly int appearanceTime;
			public readonly int beforeMissionId;

			public Condition (int appearance, int beforeMission) {
				this.appearanceTime = appearance;
				this.beforeMissionId = beforeMission;
			}
		}

		public readonly int id;
		public readonly string title;
		public readonly string summary;
		public readonly Type type;

		public readonly Award award;
		public readonly Detail detail;
		public readonly Condition condition;

		public Data (Builder builder) {
			this.id = builder.id;
			this.title = builder.title;
			this.summary = builder.summary;
			this.type = builder.type;
			this.detail = new Data.Detail(builder.subject, builder.comparion, builder.value);
			this.award = new Award(builder.awardType, builder.awardId);
			this.condition = new Data.Condition(builder.appearanceTime, builder.beforeMissionId);
		}
	}

	public Data data;
	public Type type { get { return data.type; } }
	public int id { get { return data.id; } }
	public int Count { get { return count; } }
	public float achievementRate { get { return (this.state == State.ACHIEVED) ? 1f : (float)this.count / (float)data.detail.value; } }
	public State state;

	int count;

	public Mission (Entity_Missions.Param p) {
		Type t = (Type)p.type;
		state = State.UNKNOWN;
		Builder builder = new Builder(p.id, p.title, p.summary, t);
		data = builder.award(p.award_type, p.award_id).condition(p.appearance_time, p.before_mission_id).detail(p.subject, p.comparison, p.value).build();
		count = 0;
		ChangeState();
	}

	public Mission (MissionModel model) {
		Type t = (Type)model.iType;
		state = (State)model.iState;
		Builder builder = new Builder(model.id, model.title, model.summary, t);
		data = builder.award(model.awardType, model.awardId).detail(model.subject, model.comparion, model.value).condition(model.appTime, model.beforeId).build();
		count = model.count;
		ChangeState();
	}

	public bool Update (Hashtable hash) {
		switch(data.type) {
			case Type.Battle:
				return AnalyzeBattleTable(hash);
			case Type.Organize:
				break;
			case Type.Request:
				break;
			case Type.Other:
				break;
			default:
				break;
		}
		return false;
	}

	public void ChangeState () {
		switch(state) {
			case State.CHALLENGING:
				if(achievementRate >= 1f) state = State.ACHIEVED;
				break;

			case State.UNKNOWN:
			case State.NOT_RECEIVED:
				int gameProgress = PlayerInfo.Instance.GameProgress;
				// ゲーム進行度が出現時期を満たしている
				if(gameProgress >= data.condition.appearanceTime) {
					if(data.condition.beforeMissionId != 0) {
						// 事前に達成しておくべきミッションがあれば参照して達成済みか調べて
						MissionManager manager = MissionManager.Instance;
						Mission beforeMission = manager.GetMission(data.condition.beforeMissionId);
						if(beforeMission == null) {
							Debug.Log("BeforeMission null" + data.condition.beforeMissionId);
							return;
						}
						if(beforeMission.state == State.ACHIEVED) {
							// 達成してたらミッションを受ける
							this.state = State.CHALLENGING;
						}
						return;
					}

					// 特に条件がなければ受ける
					this.state = State.CHALLENGING;
					return;
				}
				// ゲーム進行度が出現時期を満たさないなら受けない
				this.state = State.NOT_RECEIVED;
				return;

			// 達成済みの場合何もしない
			case State.ACHIEVED:
			default:
				return;
		}
	}

	/** GetMissionDataString()
	 * 
	public string GetMissionDataString () {
		Debug.Log(data.summary.Replace("\n", ""));
		string[] strAry1 = {
							  "Mission" + data.id.ToString("00"),
							  data.title,
							  data.summary.Replace("\n", ""),
							  ((int)data.type).ToString("00"),
							  count.ToString("00")
						  };
		string[] strAry2 = {							   
							  data.award.type.ToString("00"),
							  data.award.id.ToString("00"),
							  data.detail.subject,
							  data.detail.comparion,
							  data.detail.value.ToString("00"),
							  data.condition.appearanceTime.ToString("00"),
							  data.condition.beforeMissionId.ToString("00")
						   };

		string s1 = System.String.Join(",", strAry1);
		string s2 = System.String.Join(",", strAry2);
		string[] strAry3 = {s1, s2};

		return System.String.Join("/", strAry3);
	}
	 * 
	 */

	bool AnalyzeBattleTable (Hashtable tb) {
		switch(data.detail.subject) {
			case "BEAT":
				if((bool)tb["isPlayerWin"]) return CheckValue(++count);
				break;
			default:
				break;
		}
		return false;
	}

	bool CheckValue (int value) {
		int v = data.detail.value;
		string sign = data.detail.comparion;
		switch(sign) {
			case "GE":
				return (value >= v);
			case "LE":
				return (value <= v);
			case "EQ":
				return (value == v);
			default:
				return false;
		}
	}
}
