﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

public class MissionManager : SingletonMonoBehaviour<MissionManager> {

	List<Mission> missionList = null;
	Queue<Mission> achieved;

	void Start () {
		UICanvas canvas = UICanvas.Instance;
		canvas.AddPrefab("NoticeDialog", "Prefabs/UI/NoticeDialog", true);
		achieved = new Queue<Mission>();

		//missionList = new List<Mission>();
		missionList = Load();
		if(missionList == null) missionList = new List<Mission>();
		if(missionList.Count == 0) {
			missionList = ImportMtbMissions();
			Save(missionList);
		}
		else {
			missionList = ImportMtbMissions();
		}

		UpdateAllState();
	}

	void UpdateAllState () {
		if(missionList == null) return;
		if(missionList.Count == 0) return;
		foreach(Mission m in missionList) {
			m.ChangeState();
		}
		Save(missionList);
	}

	public List<Mission> GetChallengingMission () {
		if(missionList == null) return null;
		if(missionList.Count == 0) return null;
		return missionList.FindAll((m) => m.state == Mission.State.CHALLENGING);
	}

	public void BeatEnemy (Hashtable result) {
		List<Mission> list = missionList.FindAll((m) => (m.type == Mission.Type.Battle) && (m.state == Mission.State.CHALLENGING));
		foreach(Mission m in list) {
			if(m.Update(result)) {
				Debug.Log("Mission cleared!!");
				achieved.Enqueue(m);
			}
		}
		UpdateAllState();
	}

	public void CheckAchieved () {
		UpdateAllState();
		if(achieved.Count > 0) GoSceneDutyDialog();
	}

	public void ShowGetAwardDialog () {
		if(achieved.Count == 0) return;
		StartCoroutine(ShowGetAwardDialogCo());
	}

	IEnumerator ShowGetAwardDialogCo () {
		UICanvas canvas = UICanvas.Instance;
		while(canvas.Base.FindChild("SelectView(Clone)") == null) yield return null;
		GameObject p = canvas.GetPrefab("NoticeDialog");
		if(p == null) {
			canvas.AddPrefab("NoticeDialog", "Prefabs/UI/NoticeDialog", true);
			p = canvas.GetPrefab("NoticeDialog");
		}
		GameObject go = Instantiate(p) as GameObject;
		go.transform.SetParent(canvas.Base, false);
		NoticeDialog dialog = go.GetComponent<NoticeDialog>();

		Mission m = achieved.Dequeue();

		dialog.Set("ミッションを達成しました", "受け取る", () => GetMissionAward(m, dialog));
	}

	void GetMissionAward (Mission mission, NoticeDialog dialog) {
		DebugText.Instance.Log("AwardId:" + mission.data.award.id + ", AwardType:" + mission.data.award.type);
		DebugText.Instance.Log("報酬を受け取りました(未実装)");
		dialog.DestroyAnimation();
		ShowGetAwardDialog();
	}

	void GoSceneDutyDialog () {
		UICanvas canvas = UICanvas.Instance;
		GameObject p = canvas.GetPrefab("NoticeDialog");
		if(p == null) {
			canvas.AddPrefab("NoticeDialog", "Prefabs/UI/NoticeDialog", true);
			p = canvas.GetPrefab("NoticeDialog");
		}
		GameObject go = Instantiate(p) as GameObject;
		go.transform.SetParent(canvas.Base, false);
		NoticeDialog dialog = go.GetComponent<NoticeDialog>();
		dialog.Set("達成したミッションがあります", "確認", () => BaseManager.Instance.GoSceneDuty());
	}

	public Mission GetMission (int id) {
		if(missionList == null) return null;
		if(missionList.Count == 0) return null;
		return missionList.Find((m) => m.id == id);
	}

	List<Mission> ImportMtbMissions () {
		if(missionList == null) missionList = new List<Mission>();
		Entity_Missions entityMissions = Resources.Load("Data/mtb_missions") as Entity_Missions;
		for(int i = 0; i < entityMissions.sheets[0].list.Count; i++) {
			Entity_Missions.Param data = entityMissions.sheets[0].list[i];
			if(GetMission(data.id) == null) {
				Debug.Log("Data not found->" + data.id);
				Mission mission = new Mission(data);
				missionList.Add(mission);
			}
		}
		return missionList;
	}

	public bool DeleteMissionFile () {
		string filePath = GetFilePath();
		try {
			File.Delete(filePath);
		}
		catch(IOException e) {
			DebugText.Instance.Log(e.Message);
			return false;
		}

		return true;
	}

	public void Save (List<Mission> missions) {
		List<MissionModel> models = new List<MissionModel>();
		for(int i = 0; i < missions.Count; i++) {
			models.Add(new MissionModel(missions[i]));
		}
		string json = JsonMapper.ToJson(models);
		json += "[END]"; // 復号化の際にPaddingされたデータを除去するためのデリミタの追記
		string crypted = Crypt.Encrypt(json);
		string filePath = GetFilePath();
		FileStream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
		BinaryWriter writer = new BinaryWriter(fileStream);
		writer.Write(crypted);
		//writer.Write(json);
		writer.Close();
	}

	// via http://www.atmarkit.co.jp/fdotnet/dotnettips/669bincopy/bincopy.html
	public List<Mission> Load () {
		string filePath = GetFilePath();
		try {
			FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
			BinaryReader reader = new BinaryReader(fileStream);
			List<MissionModel> models = new List<MissionModel>();
			if(reader != null) {
				string str = reader.ReadString();
				string decrypted = Crypt.Decrypt(str);
				decrypted = System.Text.RegularExpressions.Regex.Replace(decrypted, @"\[END\].*$", "");
				MissionModel[] datas = JsonMapper.ToObject<MissionModel[]>(decrypted);
				//MissionModel[] datas = JsonMapper.ToObject<MissionModel[]>(str);
				models.AddRange(datas);
				reader.Close();
			}

			if(missionList == null) missionList = new List<Mission>();
			while(models.Count > 0) {
				missionList.Add(new Mission(models[0]));
				models.RemoveAt(0);
			}

			return missionList;
		}
		catch(System.Exception e) {
			DebugText.Instance.Log(e.Message);
			return new List<Mission>();
		}
	}

	string GetFilePath () {
		string fileName = "MissionData";
		return Application.persistentDataPath + "/" + fileName;
	}

	// via http://yukimemo.hatenadiary.jp/entry/2014/04/15/023802
	private class Crypt {

		private const string AesIV = @"jCddaOybW3zEh0Kl";
		private const string AesKey = @"giVJrbHRlWBDIggF";

		public static string Encrypt (string text) {

			RijndaelManaged aes = new RijndaelManaged();
			aes.BlockSize = 128;
			aes.KeySize = 128;
			aes.Padding = PaddingMode.Zeros;
			aes.Mode = CipherMode.CBC;
			aes.Key = System.Text.Encoding.UTF8.GetBytes(AesKey);
			aes.IV = System.Text.Encoding.UTF8.GetBytes(AesIV);

			ICryptoTransform encrypt = aes.CreateEncryptor();
			MemoryStream memoryStream = new MemoryStream();
			CryptoStream cryptStream = new CryptoStream(memoryStream, encrypt, CryptoStreamMode.Write);

			byte[] text_bytes = System.Text.Encoding.UTF8.GetBytes(text);

			cryptStream.Write(text_bytes, 0, text_bytes.Length);
			cryptStream.FlushFinalBlock();

			byte[] encrypted = memoryStream.ToArray();

			return (System.Convert.ToBase64String(encrypted));
		}

		public static string Decrypt (string cryptText) {

			RijndaelManaged aes = new RijndaelManaged();
			aes.BlockSize = 128;
			aes.KeySize = 128;
			aes.Padding = PaddingMode.Zeros;
			aes.Mode = CipherMode.CBC;
			aes.Key = System.Text.Encoding.UTF8.GetBytes(AesKey);
			aes.IV = System.Text.Encoding.UTF8.GetBytes(AesIV);

			ICryptoTransform decryptor = aes.CreateDecryptor();

			byte[] encrypted = System.Convert.FromBase64String(cryptText);
			byte[] planeText = new byte[encrypted.Length];

			MemoryStream memoryStream = new MemoryStream(encrypted);
			CryptoStream cryptStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

			cryptStream.Read(planeText, 0, planeText.Length);

			return (System.Text.Encoding.UTF8.GetString(planeText));
		}
	}
}
