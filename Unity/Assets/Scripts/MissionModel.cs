﻿using UnityEngine;
using System.Collections;

public class MissionModel {
	public int id;
	public string title;
	public string summary;
	public int iType;
	public int awardType;
	public int awardId;
	public string subject;
	public string comparion;
	public int value;
	public int appTime;
	public int beforeId;
	public int count;
	public int iState;

	public MissionModel () { }

	public MissionModel (Mission m) {
		id = m.id;
		title = m.data.title;
		summary = m.data.summary;
		iType = (int)m.type;
		awardType = m.data.award.type;
		awardId = m.data.award.id;
		subject = m.data.detail.subject;
		comparion = m.data.detail.comparion;
		value = m.data.detail.value;
		appTime = m.data.condition.appearanceTime;
		beforeId = m.data.condition.beforeMissionId;
		count = m.Count;
		iState = (int)m.state;
	}
}
