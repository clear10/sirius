﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ModelTakedOverCanvas {

	Dictionary<string, GameObject> prefabs;
	Dictionary<string, string> resourcePaths;

	List<string> prefabKeys;
	List<string> reuseKeys;
	List<GameObject> dontDestroyObjs;

	public ModelTakedOverCanvas (
		Dictionary<string, GameObject> prefabs,
		Dictionary<string, string> resourcePaths,
		List<string> prefabKeys,
		List<string> reuseKeys,
		List<GameObject> dontDestroyObjs
		)
	{
		this.prefabs = prefabs;
		this.resourcePaths = resourcePaths;
		this.prefabKeys = prefabKeys;
		this.reuseKeys = reuseKeys;
		this.dontDestroyObjs = dontDestroyObjs;
	}


	public Dictionary<string, GameObject> Prefabs { get { return prefabs; } }
	public Dictionary<string, string> ResourcePaths { get { return resourcePaths; } }
	public List<string> PrefabKeys { get { return prefabKeys; } }
	public List<string> ReuseKeys { get { return reuseKeys; } }
	public List<GameObject> DontDestroyObjs { get { return dontDestroyObjs; } }
}
