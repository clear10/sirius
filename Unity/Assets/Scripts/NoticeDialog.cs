﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NoticeDialog : MonoBehaviour {

	[SerializeField]
	Button button;
	[SerializeField]
	Text text;

	// Use this for initialization
	void Start () {
		ShowAnimation();
	}

	public void Set (string summary, string btn, UnityEngine.Events.UnityAction call) {
		button.onClick.AddListener(call);
		button.transform.GetChild(0).GetComponent<Text>().text = btn;
		text.text = summary;
	}

	public void ShowAnimation () {
		StartCoroutine(ShowAnimationCo(0.1f));
	}

	IEnumerator ShowAnimationCo (float t) {
		RectTransform rect = GetComponent<RectTransform>();
		int frame = 20;
		Vector3 origin = rect.localScale;
		Color c = GetComponent<RawImage>().color;

		for(int i = 0; i < frame; i++) {
			Vector3 s = Vector3.Lerp(Vector3.zero, origin, (float)i / (float)frame);
			rect.localScale = s;
			c = new Color(c.r, c.g, c.b, 255f * s.x);
			yield return new WaitForSeconds(t / (float)frame);
		}
	}

	public void DestroyAnimation () {
		StartCoroutine(DestroyAnimationCo(0.1f));
	}

	IEnumerator DestroyAnimationCo (float t) {
		RectTransform rect = GetComponent<RectTransform>();
		int frame = 20;
		Vector3 origin = rect.localScale;
		Color c = GetComponent<RawImage>().color;
		for(int i = 0; i < frame; i++) {
			Vector3 s = Vector3.Lerp(origin, Vector3.zero, (float)i/(float)frame);
			rect.localScale = s;
			c = new Color(c.r, c.g, c.b, 255f *(1f - s.x));
			yield return new WaitForSeconds(t / (float)frame);
		}
		Destroy(this.gameObject);
	}
}
