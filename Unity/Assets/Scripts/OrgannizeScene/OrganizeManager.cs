﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OrganizeManager : SingletonMonoBehaviour<OrganizeManager> {

	public Vector3 tileOrigin;

	Dictionary<string, Vector2> DictOrganization;
	Dictionary<string, UnitInfo> DictInformation;
	int[] organization;

	int organizedUnitCount = 0;

	// Use this for initialization
	void Start () {
		//gameObject.AddComponent
		organization = new int[3 * 4];
		for(int i = 0; i < 3 * 4; i++)
			organization[i] = 0;
		DictOrganization = new Dictionary<string, Vector2>();
		//DebugText.Instance.Log(organization[0, 0]);
		//GameManager.Instance.SendMessage("OnLoadSceneOrganize", this, SendMessageOptions.DontRequireReceiver);
		GameManager.Instance.OnLoadSceneOrganize(this);
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			//scroll.AddNode2Content();
		}
	}

	public void Load (List<UnitInfo> units) {
		Vector2 errorPos = new Vector2(-1, -1);
		Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
		DictInformation = new Dictionary<string, UnitInfo>();
		OrganizeUIManager ui = this.GetComponent<OrganizeUIManager>();
		UICanvas.Instance.AddPrefab("OrganizedUnit", "Prefabs/OrganizedUnit");
		GameObject obj = UICanvas.Instance.GetPrefab("OrganizedUnit"); //new GameObject(info.GetName());
		foreach(UnitInfo info in units) {
			DictInformation.Add(info.GetID(), info);
			string path = info.GetSpritePath();
			if(!sprites.ContainsKey(path)) {
				//DebugText.Instance.Log("Found: " + info.GetJobClass().ToString());
				Sprite sprite = Resources.Load<Sprite>(path);
				sprites.Add(path, sprite);
			}
			if(info.GetTilePosition() == errorPos) {
				ui.AddNode2Content(info.GetID(), sprites[path]);
			}
			else {
				PutUnitOn((int)info.GetTilePosition().x, (int)info.GetTilePosition().y, info.GetID());
				float ofs_x = 0;
				float ofs_y = 0.5f;
				Vector3 pos = new Vector3(info.GetTilePosition().x + ofs_x, -info.GetTilePosition().y + ofs_y);
				GameObject go = Instantiate(obj, pos, Quaternion.identity) as GameObject; //obj.transform.position = pos;
				go.name = info.GetName();
				SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
				sr.sprite = sprites[path];
				OrganizedUnit ounit = go.GetComponent<OrganizedUnit>();
				ounit.enabled = true;
				ounit.Init(info.GetID(), (int)info.GetTilePosition().x, (int)info.GetTilePosition().y);
			}
		}
		SelectUnit("Unit01");
	}

	public void GoSceneSelectEnemy () {
		if(organizedUnitCount <= 0) {
			DebugText.Instance.Log("Did not organized!!");
			return;
		}

		UICanvas canvas = UICanvas.Instance;
		//canvas.ForceDestroy("BasePanel");
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		if(PlayerInfo.Instance.SaveOrganization(DictOrganization))
			FadeManager.Instance.LoadLevel("SelectEnemy", 0.1f);

		//DebugText.Instance.Log("GoSceneBattle");
	}

	public void GoSceneBook () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		if(PlayerInfo.Instance.SaveOrganization(DictOrganization))
			FadeManager.Instance.LoadLevel("Book", 0.1f);
	}

	public void GoSceneBase () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		if(PlayerInfo.Instance.SaveOrganization(DictOrganization))
			FadeManager.Instance.LoadLevel("Base", 0.1f);
	}

	public void GoSceneDuty () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Duty", 0.1f);
	}

	public bool CheckPutUnitOn (int x, int y, bool isOrganized = false) {
		/**
		 * その座標にユニットが配置されていない かつ 配置済みユニットの合計が7人以下 => true
		 * ただし編成済みユニットの移動の場合もあるのでそこのチェックをする
		 */
		bool ret = isOrganized ? true : organizedUnitCount < 7;
		return (organization[x + y * 4] == 0 && ret) ? true : false;
	}

	public void PutUnitOn (int x, int y, string id) {
		organization[x + y * 4] = 1;
		DictOrganization[id] = new Vector2(x, y);
		organizedUnitCount++;
	}

	public void RemoveUnitOn (int x, int y, string id) {
		if(DictOrganization[id] != new Vector2(x, y)) return;

		//DictOrganization.Remove(id);
		DictOrganization[id] = new Vector2(-1, -1);
		organization[x + y * 4] = 0;
		organizedUnitCount--;
	}

	public void SelectUnit (string id) {
		OrganizeUIManager ui = this.GetComponent<OrganizeUIManager>();
		ui.UpdateUnitDetail(DictInformation[id]);
	}

	public bool CallSaveOrg (PlayerInfo player) {
		return player.SaveOrganization(DictOrganization);
	}
}
