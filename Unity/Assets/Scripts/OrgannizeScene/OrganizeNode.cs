﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OrganizeNode : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler {

	Image image;
	GameObject tileObject;
	ScrollRect sr;
	bool isInstantiated;
	bool isWriteable = true;
	string id;

	public string ID {
		get { return id; }
		set { if(isWriteable) { id = value; isWriteable = false; } }
	}

	// Use this for initialization
	void Start () {
		image = GetComponent<Image>();
		//tileObject = GetTileObject();
		sr = transform.parent.parent.gameObject.GetComponent<ScrollRect>();
		isInstantiated = false;
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnBeginDrag (PointerEventData ped) {
		//gameObject.SetActive(false);
		sr.OnBeginDrag(ped);
		Vector3 pos = Camera.main.ScreenToWorldPoint(ped.position);
		pos.Set(pos.x, pos.y, 0);
		//DebugText.Instance.Log("マウスドラッグ開始 position=" + pos);
		//DebugText.Instance.Log(id);

		OrganizeManager om = OrganizeManager.Instance;
		om.SelectUnit(id);
	}
	public void OnDrag (PointerEventData ped) {
		Vector3 pos = Camera.main.ScreenToWorldPoint(ped.position);
		pos.Set(pos.x, pos.y, 0);
		//DebugText.Instance.Log("マウスドラッグ中 position=" + pos);
		if(!isInstantiated) {
			sr.OnDrag(ped);
			if(ped.position.y > ped.pressPosition.y + 50) {
				image.enabled = false;
				tileObject = GetTileObject();
				tileObject = (GameObject)Instantiate(tileObject, pos, Quaternion.identity);
				isInstantiated = true;
			}
		}
		else tileObject.transform.position = pos;
	}
	public void OnEndDrag (PointerEventData ped) {
		//gameObject.SetActive(true);
		//Destroy(tileObject);

		sr.OnEndDrag(ped);

		if(isInstantiated) {
			Vector3 pos = tileObject.transform.position;
			pos.Set(pos.x, pos.y, 0);
			//DebugText.Instance.Log("マウスドラッグ終了 position=" + pos);
			float ofs_x = 0;
			float ofs_y = 0.5f;
			if(pos.x > -0.4f + ofs_x && pos.x < 3.4f + ofs_x && pos.y > -2.4f + ofs_y && pos.y < 0.4f + ofs_y) {
				int x = Mathf.RoundToInt(pos.x - ofs_x);
				int y = -Mathf.RoundToInt(pos.y - ofs_y);
				DebugText.Instance.Log(x.ToString() + "," + y.ToString());
				OrganizeManager om = OrganizeManager.Instance;
				bool ret = om.CheckPutUnitOn(x, y);
				if(ret) {
					om.PutUnitOn(x, y, id);
					tileObject.transform.position = new Vector3(x + ofs_x, -y + ofs_y, 0);
					OrganizedUnit ounit = tileObject.GetComponent<OrganizedUnit>();
					ounit.enabled = true;
					ounit.Init(this.id, x, y);
					StartCoroutine(DestroyAfterFrame(1));
					return;
				}
			}

			Destroy(tileObject);
			image.enabled = true;
			isInstantiated = false;
			tileObject = GetTileObject();
		}
	}

	public void OnPointerClick (PointerEventData ped) {

		OrganizeManager om = OrganizeManager.Instance;
		Debug.Log(ID);
		om.SelectUnit(id);
	}

	IEnumerator DestroyAfterFrame (int n) {
		for(int i = 0; i < n; i++) {
			yield return null;
		}
		Destroy(this.gameObject);
	}

	GameObject GetTileObject () {
		//DebugText.Instance.Log(image.sprite.ToString());

		UICanvas.Instance.AddPrefab("OrganizedUnit", "Prefabs/OrganizedUnit");
		GameObject obj = UICanvas.Instance.GetPrefab("OrganizedUnit");	//Resources.Load<GameObject>("Prefabs/OrganizedUnit"); //new GameObject(info.GetName());
		OrganizedUnit ounit = obj.GetComponent<OrganizedUnit>();
		ounit.enabled = false;
		obj.name = id;
		SpriteRenderer rend = obj.GetComponent<SpriteRenderer>();
		rend.sprite = image.sprite;

		return obj;

		/*
		switch(image.sprite.name) {
			case "f017":
				return (GameObject)Resources.Load("Prefabs/icon_knight");
			case "f021":
				return (GameObject)Resources.Load("Prefabs/icon_fighter");
			case "f019":
				return (GameObject)Resources.Load("Prefabs/icon_wizard");
			case "f011":
				return (GameObject)Resources.Load("Prefabs/icon_thief");
			case "f073":
				return (GameObject)Resources.Load("Prefabs/icon_priest");
			case "f010":
				return (GameObject)Resources.Load("Prefabs/icon_monster");
			default:
				return null;
		}
		 */
	}
}
