﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class OrganizeUIManager : UIController {

	class NodeInfo {
		string id;
		Sprite sprite;
		public NodeInfo (string id, Sprite sprite) {
			this.id = id;
			this.sprite = sprite;
		}

		public string ID { get { return id; } }
		public Sprite Sprite { get { return sprite; } }
	}

	UnitDetailPanel detailPanel = null;
	ScrollController scroll = null;
	Queue<NodeInfo> nodeQueue = new Queue<NodeInfo>();
	Queue<UnitInfo> nodeUnitInfo = new Queue<UnitInfo>();

	protected override void RegistPrefabs (UICanvas canvas) {
		canvas.AddPrefab("CommonButton", "Prefabs/UI/CommonButton", true);
		canvas.AddPrefab("UnitDetailPanel", "Prefabs/UI/UnitDetailPanel");
		canvas.AddPrefab("ScrollView", "Prefabs/UI/ScrollView");
	}

	protected override void ShowUI (UICanvas canvas, Transform parent) {
		GameObject pnlUnitDetail = Instantiate(canvas.GetPrefab("UnitDetailPanel")) as GameObject;
		pnlUnitDetail.transform.SetParent(parent, false);
		detailPanel = pnlUnitDetail.GetComponent<UnitDetailPanel>();
		GameObject scrollView = Instantiate(canvas.GetPrefab("ScrollView")) as GameObject;
		scrollView.transform.SetParent(parent, false);
		scroll = scrollView.transform.GetChild(0).GetComponent<ScrollController>();
		/*
		GameObject btn2base = Instantiate(canvas.GetPrefab("CommonButton")) as GameObject;
		RectTransform rect = btn2base.GetComponent<RectTransform>();
		rect.sizeDelta = new Vector2(180, 60);
		rect.anchoredPosition = new Vector2((720-rect.sizeDelta.x)/2, 1280-320-rect.sizeDelta.y);
		InitButton(btn2base, "Go2Base", parent, () => manager.GoSceneBase());
		*/
		OrganizeManager manager = OrganizeManager.Instance;
		GameObject pnlBase = canvas.GetDontDestroyedObj("BasePanel");
		InitButton(pnlBase.transform.FindChild("CommonButton3").gameObject, "図鑑", pnlBase.transform, () => manager.GoSceneBook());
		InitButton(pnlBase.transform.FindChild("CommonButton2").gameObject, "編成", pnlBase.transform, null);
		InitButton(pnlBase.transform.FindChild("CommonButton0").gameObject, "戦闘", pnlBase.transform, () => manager.GoSceneSelectEnemy());
		InitButton(pnlBase.transform.FindChild("CommonButton1").gameObject, "任務", pnlBase.transform, () => manager.GoSceneDuty());
		InitButton(pnlBase.transform.FindChild("CommonButton4").gameObject, "←", pnlBase.transform, () => manager.GoSceneBase());
		pnlBase.transform.FindChild("CommonButton4").gameObject.SetActive(true);
	}

	public void UpdateUnitDetail (UnitInfo info) {
		nodeUnitInfo.Enqueue(info);
		StartCoroutine(UpdatePanelByQueue());
	}

	public void AddNode2Content (string id, Sprite sprite) {
		NodeInfo node = new NodeInfo(id, sprite);
		nodeQueue.Enqueue(node);
		StartCoroutine(AddNodeByQueue());
	}

	IEnumerator UpdatePanelByQueue () {
		while(detailPanel == null) yield return 0;

		UnitInfo info = nodeUnitInfo.Dequeue();
		detailPanel.UpdateUnitDetail(info);
	}

	IEnumerator AddNodeByQueue () {
		while(scroll == null) yield return 0;

		NodeInfo node = nodeQueue.Dequeue();
		scroll.AddNode2Content(node.ID, node.Sprite);
	}

	/*
	protected override void AdjustUITransform () {
		pnlUnitDetail.sizeDelta = new Vector2(0, screenHeight * 0.30f);
		pnlScrollView.sizeDelta = new Vector2(0, screenHeight * 0.1f);
		RectTransform content = pnlScrollView.GetChild(0).GetComponent<RectTransform>();
		content.offsetMin = new Vector2(0, screenHeight * 0.1f * 0.1f);
		content.offsetMax = new Vector2(0, screenHeight * 0.1f * 0.9f);
		//DebugText.Instance.Log(button.offsetMin);
		//DebugText.Instance.Log(button.offsetMax);
		button.offsetMin = new Vector2(screenWidth * 0.8f, -screenHeight * 0.3f * 1.3f);
		button.offsetMax = new Vector2(0, -screenHeight * 0.3f);
		//button.localPosition = new Vector3(screenWidth * 0.9f, -screenHeight * 0.15f, 0);
		//button.sizeDelta = new Vector2(0, screenHeight * 0.05f);
	}
	public void UpdateUnitDetail (UnitInfo info) {
		Image image = TextName.transform.parent.GetComponent<Image>();
		image.sprite = Resources.Load<Sprite>(info.GetSpritePath());

		Status param = info.GetParam();
		Text txt = TextName.GetComponent<Text>();
		txt.text = info.GetName();
		txt = TextHp.transform.GetChild(0).GetComponent<Text>();
		txt.text = param.Hp.ToString();
		txt = TextAtk.transform.GetChild(0).GetComponent<Text>();
		txt.text = param.Atk.ToString();
		txt = TextAgl.transform.GetChild(0).GetComponent<Text>();
		txt.text = param.Agl.ToString();
		txt = TextReg.transform.GetChild(0).GetComponent<Text>();
		txt.text = param.Reg.ToString();
		txt = TextSAtk.transform.GetChild(0).GetComponent<Text>();
		txt.text = param.SAtk.ToString();
		txt = TextSDef.transform.GetChild(0).GetComponent<Text>();
		txt.text = param.SDef.ToString();
	}
	*/
}
