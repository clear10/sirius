﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class OrganizedUnit : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {

	bool isWriteable = true;
	string id;
	Vector3 previewPos;

	int prev_x = -1;
	int prev_y = -1;

	const float picked_x = -0.1f;
	const float picked_y = 0.1f;

	public string ID {
		get { return id; }
		set { if(isWriteable) { id = value; isWriteable = false; } }
	}

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {

	}

	public void Init (string id, int x, int y) {
		this.id = id;
		prev_x = x;
		prev_y = y;
	}

	public void OnBeginDrag (PointerEventData ped) {
		//gameObject.SetActive(false);
		//Vector3 pos = Camera.main.ScreenToWorldPoint(ped.position);
		//pos.Set(pos.x + picked_x, pos.y + picked_y, 0);
		//this.transform.position = pos;
		//DebugText.Instance.Log("OUnit dragged");
		OrganizeManager om = OrganizeManager.Instance;
		om.SelectUnit(id);
	}
	public void OnDrag (PointerEventData ped) {
		Vector3 pos = Camera.main.ScreenToWorldPoint(ped.position);
		pos.Set(pos.x + picked_x, pos.y + picked_y, -1);
		this.transform.position = pos;
	}
	public void OnEndDrag (PointerEventData ped) {
		Vector3 pos = this.transform.position; //Camera.main.ScreenToWorldPoint(ped.position);
		float ofs_x = 0;
		float ofs_y = 0.5f;
		if(pos.x > -0.4f + ofs_x && pos.x < 3.4f + ofs_x && pos.y > -2.4f + ofs_y && pos.y < 0.4f + ofs_y) {
			int x = Mathf.RoundToInt(pos.x - ofs_x);
			int y = -Mathf.RoundToInt(pos.y - ofs_y);
			DebugText.Instance.Log(x.ToString() + "," + y.ToString());
			OrganizeManager om = OrganizeManager.Instance;
			bool ret = om.CheckPutUnitOn(x, y, true);
			if(ret) {
				om.RemoveUnitOn(prev_x, prev_y, id);
				om.PutUnitOn(x, y, id);
				prev_x = x;
				prev_y = y;
				this.transform.position = new Vector3(x + ofs_x, -y + ofs_y, 0);
				previewPos = transform.position;
				return;
			}
		}

		if(ped.position.y < Screen.height * 0.15f) {
			OrganizeManager om = OrganizeManager.Instance;
			om.RemoveUnitOn(prev_x, prev_y, id);
			SpriteRenderer rend = GetComponent<SpriteRenderer>();
			OrganizeUIManager ui = om.gameObject.GetComponent<OrganizeUIManager>();
			ui.AddNode2Content(id, rend.sprite);
			Destroy(this.gameObject);
		}
		else {
			transform.position = previewPos;
		}
	}

	public void OnPointerClick (PointerEventData ped) {

		OrganizeManager om = OrganizeManager.Instance;
		om.SelectUnit(id);
	}

	public void OnPointerDown (PointerEventData ped) {
		previewPos = transform.position;
		Vector3 pos = Camera.main.ScreenToWorldPoint(ped.position);
		pos.Set(pos.x + picked_x, pos.y + picked_y, -1);
		this.transform.position = pos;
	}

	public void OnPointerUp (PointerEventData ped) {
		if(ped.dragging) return;
		transform.position = previewPos;
	}
}
