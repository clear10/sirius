﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollController : MonoBehaviour {

	[SerializeField]
	RectTransform prefab = null;
	bool isInited = false;

	void Start () {
	}

	/*
	public void AddNode2Content (Sprite sprite) {
		var obj = Instantiate(prefab) as GameObject;
		Image image = obj.GetComponent<Image>();
		image.sprite// = sprite;
		obj.transform.SetParent(transform, false);
		obj.SetActive(true);
	}
	 */
	public GameObject AddNode2Content (string id, Sprite sprite) {
		if(!isInited) Init();
		var item = GameObject.Instantiate(prefab) as RectTransform;
		item.SetParent(transform, false);

		var obj = item.gameObject;
		Image image = obj.GetComponent<Image>();
		if(sprite != null) image.sprite = sprite;
		OrganizeNode node = obj.GetComponent<OrganizeNode>();
		node.ID = id;
		//DebugText.Instance.Log(sprite);
		obj.SetActive(true);
		return obj;
	}

	void Init () {
		LayoutElement le = prefab.GetComponent<LayoutElement>();
		//OrganizeUIManager ui = GameObject.Find("OrganizeManager").GetComponent<OrganizeUIManager>();
		float value = GetComponent<RectTransform>().sizeDelta.y;
		le.minWidth = value; //Screen.height * 0.1f * 0.8f; //ui.GetNodeHeight();
		le.minHeight = value;
		isInited = true;
	}
}