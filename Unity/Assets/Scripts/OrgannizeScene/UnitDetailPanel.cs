﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UnitDetailPanel : MonoBehaviour {

	Image unitIcn;
	Text nameTxt;
	Text hpTxt;
	Text aglTxt;
	Text atkTxt;
	Text satkTxt;
	Text sdefTxt;
	Text regTxt;

	// Use this for initialization
	void Start () {
		unitIcn = transform.FindChild("Image").GetComponent<Image>();
		Transform iconTran = unitIcn.gameObject.transform;
		nameTxt = iconTran.FindChild("Name").GetComponent<Text>();
		hpTxt = iconTran.FindChild("Hp").GetChild(0).GetComponent<Text>();
		atkTxt = iconTran.FindChild("Attack").GetChild(0).GetComponent<Text>();
		aglTxt = iconTran.FindChild("Agility").GetChild(0).GetComponent<Text>();
		regTxt = transform.FindChild("Reg").GetChild(0).GetComponent<Text>();
		satkTxt = transform.FindChild("SAtk").GetChild(0).GetComponent<Text>();
		sdefTxt = transform.FindChild("SDef").GetChild(0).GetComponent<Text>();
	}

	public void UpdateUnitDetail (UnitInfo info) {
		unitIcn.sprite = Resources.Load<Sprite>(info.GetSpritePath());
		nameTxt.text = info.GetName();
		Status param = info.GetParam();
		hpTxt.text = param.Hp.ToString();
		aglTxt.text = param.Agl.ToString();
		atkTxt.text = param.Atk.ToString();
		regTxt.text = (info.GetRegType() != Unit.RegenerateType.None) ? param.Reg.ToString() + "(" + info.GetRegType().ToString() + ")" : "None";
		satkTxt.text = (param.SAtk == 0) ? "None" : param.SAtk.ToString();
		sdefTxt.text = (param.SDef == 0) ? "None" : param.SDef.ToString();
	}
}
