﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInfo : SingletonMonoBehaviour<PlayerInfo> {
	private List<UnitInfo> team = new List<UnitInfo>();
	private MonsterInfo selectedMonsterInfo;

	#region public method & property
	public void PrepareSceneOrganize (OrganizeManager om) {
		om.Load(team);
	}

	public void PrepareSceneMain (MainManager mm) {

		Vector2 errorPos = new Vector2(-1, -1);
		foreach(UnitInfo info in team) {
			//DebugText.Instance.Log(info.GetUnitInfoString());
			if(info.GetTilePosition() == errorPos) continue;
			Unit unit = InstantiateFromUnitInfo(info, mm.tileOrigin);
			if(unit == null) continue;
			mm.AddToParty(unit.gameObject);
		}

		mm.CopyMonsterInfo(selectedMonsterInfo);
		UnitInfo monster_info = new UnitInfo(selectedMonsterInfo); //new UnitInfo("Monster00", "Daemon", UnitInfo.JobClass.Monster, new Vector2(0, 0), new Status(110, 10, 5, 0, 0, 0));

		Unit mon = InstantiateFromUnitInfo(monster_info, new Vector3(1.5f, 2.5f, 0)); //CreateUnit(JobClass.Monster, "Daemon", new Vector2(0, 0));
		mm.AddToParty(mon.gameObject);
		//selectedMonsterInfo = null;
	}

	public void Save () {
		WriteUnitsData(team);
	}

	public bool SaveOrganization (Dictionary<string, Vector2> dict) {
		foreach(string key in dict.Keys) {
			UnitInfo unit = team.Find(t => t.GetID() == key);
			if(unit != null) unit.SetTilePosition(dict[key]);
		}
		/**
		for(int i = 0; i < 3 * 4; i++) {
			string id = "Unit" + i.ToString("00");
			if(dict.ContainsKey(id)) {
				UnitInfo unit = team[i];
				unit.SetTilePosition(dict[id]);
			}
		}
		 **/
		try {
			WriteUnitsData(team);
		}
		catch(PlayerPrefsException e) {
			Debug.LogException(e);
			DebugText.Instance.Log(e.Message);
			return false;
		}
		return true;
	}

	public bool IsOrganized {
		get {
			var errPos = new Vector2(-1, -1);
			foreach(UnitInfo info in team) {
				if(info.GetTilePosition() != errPos) return true;
			}
			return false;
		}
	}

	public MonsterInfo SelectedMonster { get { return selectedMonsterInfo; } }

	public UnitInfo GetUnitInfo (string id) {
		foreach(UnitInfo info in team) {
			if(info.GetID() == id) return info;
		}
		return null;
	}

	public UnitInfo GetRandomUnitInfo () {
		return team[Random.Range(0, team.Count)];
	}

	public void SelectMonster (MonsterInfo info) {
		selectedMonsterInfo = info;
	}

	public int GameProgress { get { return PlayerPrefs.HasKey("GameProgress") ? PlayerPrefs.GetInt("GameProgress") : -1; } }

	public void SaveGameProgress (int value) {
		PlayerPrefs.SetInt("GameProgress", value);
	}

	public void RefreshUnits () {
		List<UnitInfo> add = ImportMtbUnits();
		foreach(UnitInfo info in add) {
			//Debug.Log(info.GetName());
			if(info != null)
				team.Add(info);
		}
		WriteUnitsData(team);
	}
	#endregion

	// Use this for initialization
	void Start () {
		if(!PlayerPrefs.HasKey("ExistData")) {
			PlayerPrefs.SetInt("ExistData", 1); //値はどうでもいい
			SaveGameProgress(0);
			team = ImportMtbUnits();
			WriteUnitsData(team);
			//PlayerPrefs.DeleteAll();
			return;
		}

		for(int i = 0; i < 12; i++) {
			UnitInfo info = ReadUnitData(i);
			if(info != null) team.Add(info);
		}
	}

	List<UnitInfo> ImportMtbUnits () {
		Entity_Unit entityUnit = Resources.Load("Data/Units") as Entity_Unit;
		List<UnitInfo> units = new List<UnitInfo>();
		//int id = 0;
		int myGameProgress = this.GameProgress;
		for(int i = 0; i < entityUnit.param.Count; i++) {
			Entity_Unit.Param param = entityUnit.param[i];
			if(!param.enable) continue;
			if(param.appearance_time > myGameProgress) continue;
			if(PlayerPrefs.HasKey("Unit" + param.id.ToString("00"))) continue;
			string jobStr = param.job;

			Status status = new Status(param.hp, param.atk, param.agl,
										param.s_atk, param.s_def, param.reg);

			UnitInfo info = new UnitInfo("Unit" + param.id.ToString("00"), param.name, jobStr, new Vector2(-1, -1), status);
			units.Add(info);
		}
		return units;
	}

	void WriteUnitsData (List<UnitInfo> units) {
		foreach(UnitInfo u in units) {
			string s = u.GetUnitInfoString();

			//DebugText.Instance.Log(s);

			PlayerPrefs.SetString(u.GetID(), s);
		}
	}

	UnitInfo ReadUnitData (string key) {
		string[] data = key.Split(',');

		string id = data[0];
		string name_ = data[2];
		int hp = int.Parse(data[3]);
		int atk = int.Parse(data[4]);
		int agl = int.Parse(data[5]);
		int s_atk = int.Parse(data[6]);
		int s_def = int.Parse(data[7]);
		int reg = int.Parse(data[8]);

		int x = int.Parse(data[9]);
		int y = int.Parse(data[10]);

		string jobStr = data[1];

		UnitInfo info = new UnitInfo(id, name_, jobStr, new Vector2(x, y), new Status(hp, atk, agl, s_atk, s_def, reg));
		return info;
	}

	UnitInfo ReadUnitData (int i) {
		string key = "Unit" + i.ToString("00");
		if(!PlayerPrefs.HasKey(key)) return null;

		string s = PlayerPrefs.GetString(key);
		return ReadUnitData(s);
	}

	Unit InstantiateFromUnitInfo (UnitInfo source, Vector3 tileOrigin) {
		Unit unit = null;
		GameObject obj = new GameObject();
		obj.SetActive(false);
		unit = source.AddJobComponent(obj);
		if(unit == null) {
			Destroy(obj);
			return null;
		}
		StartCoroutine(InitializeUnit(unit, source, tileOrigin));
		return unit;
	}

	IEnumerator InitializeUnit (Unit unit, UnitInfo source, Vector3 tileOrigin) {
		yield return null;

		//DebugText.Instance.Log("Initialize" + source.GetName());
		unit.Awake();
		unit.PublishID(source.GetID());
		unit.SetParam(source.GetParam());
		unit.gameObject.SetActive(true);
		unit.Init(source.GetName(), source.GetTilePosition(), tileOrigin);
	}
}
