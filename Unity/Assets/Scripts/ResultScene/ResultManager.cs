﻿using UnityEngine;
using System.Collections;

public class ResultManager : SingletonMonoBehaviour<ResultManager> {

	void Start () {
		GameManager.Instance.OnLoadSceneResult(this);
	}

	public void GoSceneBase () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Base", 0.1f);
	}

	public void GoSceneAdventure () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Adventure", 0.1f);
	}

	public void ShowReturnBtn (GameObject panel, bool isPlayerWin) {
		Destroy(panel);
		GetComponent<ResultUIManager>().ShowButton2Base(isPlayerWin);
	}

	public void StepProgress () {
		PlayerInfo player = PlayerInfo.Instance;
		MonsterInfo monster = player.SelectedMonster;
		if(monster.Appearance == player.GameProgress) {
			player.SaveGameProgress(monster.Appearance + 1);
			player.RefreshUnits();
		}
	}
}
