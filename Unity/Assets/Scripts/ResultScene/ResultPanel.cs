﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResultPanel : MonoBehaviour {

	[SerializeField]
	Text winLoseText;
	[SerializeField]
	Text passedTurnText;
	[SerializeField]
	Text rankText;
	[SerializeField]
	Text scoreText;
	[SerializeField]
	Image mvpIcon;
	[SerializeField]
	Text mvpName;
	[SerializeField]
	Text mvpDetail;
	[SerializeField]
	Image strikerIcon;
	[SerializeField]
	Text strikerName;
	[SerializeField]
	Text strikerDetail;
	[SerializeField]
	Button closeBtn;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void SetPanelDetail () {
		//経過ターン：
		BattleResult result = GameObject.Find("ResultInfo").GetComponent<BattleResult>();

		winLoseText.text = result.isPlayerWin ? "WIN!!" : "LOSE!!";
		passedTurnText.text = "経過ターン：" + result.passedTurn.ToString();
		//rankText
		//scoreText
		UnitInfo mvpInfo = PlayerInfo.Instance.GetUnitInfo(result.myMaxDamageID);
		if(mvpInfo != null) {
			mvpIcon.sprite = Resources.Load<Sprite>(mvpInfo.GetSpritePath());
			mvpName.text = mvpInfo.GetName();
			mvpDetail.text = "Highest: " + result.myMaxDamage.ToString();
		}
		if(result.isPlayerWin) {
			strikerIcon.sprite = Resources.Load<Sprite>(PlayerInfo.Instance.GetUnitInfo(result.strikerID).GetSpritePath());
			strikerName.text = PlayerInfo.Instance.GetUnitInfo(result.strikerID).GetName();
			strikerDetail.text = "JustKill: " + (result.isJustKill ? "Succeed" : "Failure");
		}
		else {
			strikerIcon.sprite = null;
			strikerName.text = null;
			strikerDetail.text = null;
		}

		Destroy(result.gameObject);

		ResultManager manager = ResultManager.Instance;
		if(result.isPlayerWin) manager.StepProgress();

		closeBtn.onClick.AddListener(() => manager.ShowReturnBtn(this.gameObject, result.isPlayerWin));
	}
}
