﻿using UnityEngine;
using System.Collections;

public class ResultUIManager : UIController {

	GameObject btn2baseObj = null;

	protected override void RegistPrefabs (UICanvas canvas) {
		canvas.AddPrefab("CommonButton", "Prefabs/UI/CommonButton", true);
		canvas.AddPrefab("ResultPanel", "Prefabs/UI/ResultPanel");
	}

	protected override void ShowUI (UICanvas canvas, Transform parent) {

		btn2baseObj = Instantiate(canvas.GetPrefab("CommonButton")) as GameObject;
		RectTransform rect = btn2baseObj.GetComponent<RectTransform>();
		rect.Translate(new Vector3(0, -rect.sizeDelta.y, 0), Space.Self);
		ResultManager manager = ResultManager.Instance;

		InitButton(btn2baseObj, "Go2Adv", parent, () => manager.GoSceneAdventure());

		GameObject resultPnl = Instantiate(canvas.GetPrefab("ResultPanel")) as GameObject;
		resultPnl.transform.SetParent(parent, false);
		resultPnl.SetActive(false);
		ResultPanel panel = resultPnl.GetComponent<ResultPanel>();
		panel.SetPanelDetail();
		resultPnl.SetActive(true);
	}

	public void ShowButton2Base (bool isPlayerWin) {
		if(!isPlayerWin) {
			/** 
			 *  負けた場合はボタンの内容を再設定して表示
			 */
			ResultManager manager = ResultManager.Instance;
			InitButton(btn2baseObj, "撤退", btn2baseObj.transform.parent, () => manager.GoSceneBase());
		}
		StartCoroutine(ShowButton2Base(0.6f));
	}

	IEnumerator ShowButton2Base (float t) {
		int frame = 60;
		RectTransform rect = btn2baseObj.GetComponent<RectTransform>();
		Vector3 from = rect.anchoredPosition;
		Vector3 to = new Vector3(0, rect.sizeDelta.y, 0);
		to = to + from;
		for(int i = 1; i <= frame; i++) {
			rect.anchoredPosition3D = Vector3.Lerp(from, to, (float)i / (float)frame);
			yield return new WaitForSeconds(t / (float)frame);
		}
	}
}
