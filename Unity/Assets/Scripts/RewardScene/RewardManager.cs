﻿using UnityEngine;
using System.Collections;

public class RewardManager : SingletonMonoBehaviour<RewardManager> {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void GoSceneBase () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Base", 0.1f);
	}
}
