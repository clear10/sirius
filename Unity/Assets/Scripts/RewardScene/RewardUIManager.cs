﻿using UnityEngine;
using System.Collections;

public class RewardUIManager : UIController {
	protected override void RegistPrefabs (UICanvas canvas) {
		canvas.AddPrefab("CommonButton", "Prefabs/UI/CommonButton", true);
	}

	protected override void ShowUI (UICanvas canvas, Transform parent) {

		GameObject btn2baseObj = Instantiate(canvas.GetPrefab("CommonButton")) as GameObject;
		RewardManager manager = this.GetComponent<RewardManager>();

		InitButton(btn2baseObj, "Go2Base", parent, () => manager.GoSceneBase());

		GameObject resultObj = GameObject.Find("ResultInfo"); //canvas.GetDontDestroyedObj("ResultInfo");
		BattleResult result = resultObj.GetComponent<BattleResult>();
		result.ShowDebug();
	}
}
