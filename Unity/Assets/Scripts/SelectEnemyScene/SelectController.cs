﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectController : MonoBehaviour {

	[SerializeField]
	RectTransform prefab = null;
	bool isInited = false;

	public GameObject AddNode2Content (MonsterInfo info) {

		SelectEnemyManager manager = SelectEnemyManager.Instance;
		return AddNode2Content(() => manager.GoSceneAdventure(info), info.Name);
	}

	public GameObject AddNode2Content (UnityEngine.Events.UnityAction callback, string str) {
		return AddNode2Content(callback, str, "");
	}
	public GameObject AddNode2Content (UnityEngine.Events.UnityAction callback, string title, string summary) {
		if(!isInited) Init();
		var item = GameObject.Instantiate(prefab) as RectTransform;
		item.SetParent(this.transform, false);

		SelectNode node = item.GetComponent<SelectNode>();
		node.Regist(title, summary, callback);
		return item.gameObject;
	}

	public GameObject AddMissionNode2Content (Mission mission, GameObject nodePrefab, UnityEngine.Events.UnityAction callback) {
		if(!isInited) Init();
		//Debug.Log(nodePrefab);
		var item = Instantiate(nodePrefab) as GameObject;
		//Debug.Log(item);
		item.transform.SetParent(this.transform, false);

		SelectMissionNode node = item.GetComponent<SelectMissionNode>();
		node.Set(mission, callback);
		return item;
	}

	void Init () {
		//LayoutElement le = prefab.GetComponent<LayoutElement>();
		//OrganizeUIManager ui = GameObject.Find("OrganizeManager").GetComponent<OrganizeUIManager>();
		//float value = GetComponent<RectTransform>().sizeDelta.y;
		//le.minWidth = value; //Screen.height * 0.1f * 0.8f; //ui.GetNodeHeight();
		//le.minHeight = value;
		isInited = true;
	}
}
