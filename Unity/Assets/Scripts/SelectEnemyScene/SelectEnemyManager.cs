﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectEnemyManager : SingletonMonoBehaviour<SelectEnemyManager> {

	List<MonsterInfo> monInfos = new List<MonsterInfo>();

	// Use this for initialization
	void Start () {

		GameManager.Instance.OnLoadSceneSelectEnemy(this);
		SelectEnemyUIManager ui = GetComponent<SelectEnemyUIManager>();
		List<MonsterInfo> monsters = ImportMtbMonsters();
		int myGameProgress = PlayerInfo.Instance.GameProgress; //PlayerPrefs.GetInt("GameProgress");
		foreach(MonsterInfo info in monsters) {
			if(info.Appearance > myGameProgress) continue;

			ui.AddNode2Content(info);
			monInfos.Add(info);
		}
	}

	List<MonsterInfo> ImportMtbMonsters () {
		Entity_Monsters entityMonster = Resources.Load("Data/mtb_monsters") as Entity_Monsters;
		List<MonsterInfo> monsters = new List<MonsterInfo>();
		for(int i = 0; i < entityMonster.sheets[0].list.Count; i++) {
			Entity_Monsters.Param data = entityMonster.sheets[0].list[i];

			Status status = new Status(data.hp, data.atk, data.agl, data.regenerate ? data.reg : 0);
			monsters.Add(new MonsterInfo(data.id, data.name, data.appearance_time, data.beforebattle_label, data.afterbattle_label, data.filename, data.regenerate, status));
		}
		return monsters;
	}

	public void GoSceneOrganize () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Organization", 0.1f);
	}
	
	public void GoSceneBook () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Book", 0.1f);
	}

	public void GoSceneBase () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Base", 0.1f);
	}

	/**	GoSceneMain
	 * 
	public void GoSceneMain (string monsterName) {

		MonsterInfo info = monInfos.Find(m => m.Name == monsterName);
		if(info == null) {
			//error
			return;
		}

		PlayerInfo.Instance.SelectMonster(info);
		monInfos = null;

		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.ForceDestroy("BasePanel");
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Main", 0.1f);
	}
	 * 
	 */

	public void GoSceneAdventure (MonsterInfo info) {
		PlayerInfo.Instance.SelectMonster(info);
		monInfos = null;
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.ForceDestroy("BasePanel");
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Adventure", 0.1f);
	}

	public void GoSceneDuty () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		FadeManager.Instance.LoadLevel("Duty", 0.1f);
	}
}
