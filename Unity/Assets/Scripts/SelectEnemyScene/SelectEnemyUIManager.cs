﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectEnemyUIManager : UIController {

	SelectController select = null;

	protected override void RegistPrefabs (UICanvas canvas) {
		canvas.AddPrefab("SelectView", "Prefabs/UI/SelectView");
	}

	protected override void ShowUI (UICanvas canvas, Transform parent) {
		GameObject selectObj = Instantiate(canvas.GetPrefab("SelectView")) as GameObject;
		selectObj.transform.SetParent(parent, false);
		select = selectObj.transform.GetChild(0).GetComponent<SelectController>();

		SelectEnemyManager manager = SelectEnemyManager.Instance;
		GameObject pnlBase = canvas.GetDontDestroyedObj("BasePanel");
		InitButton(pnlBase.transform.FindChild("CommonButton3").gameObject, "図鑑", pnlBase.transform, () => manager.GoSceneBook());
		InitButton(pnlBase.transform.FindChild("CommonButton2").gameObject, "編成", pnlBase.transform, () => manager.GoSceneOrganize());
		InitButton(pnlBase.transform.FindChild("CommonButton0").gameObject, "戦闘", pnlBase.transform, null);
		InitButton(pnlBase.transform.FindChild("CommonButton1").gameObject, "任務", pnlBase.transform, () => manager.GoSceneDuty());
		InitButton(pnlBase.transform.FindChild("CommonButton4").gameObject, "←", pnlBase.transform, () => manager.GoSceneBase());
		pnlBase.transform.FindChild("CommonButton4").gameObject.SetActive(true);
	}

	public void AddNode2Content (MonsterInfo info) {
		StartCoroutine(AddNodeCoroutine(info));
	}

	IEnumerator AddNodeCoroutine (MonsterInfo info) {
		while(select == null) {
			Debug.Log("add node");
			yield return 0;
		}

		select.AddNode2Content(info);
	}
}