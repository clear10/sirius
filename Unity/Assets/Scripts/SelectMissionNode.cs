﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectMissionNode : SelectNode {

	[SerializeField]
	Image awardImage;
	[SerializeField]
	Text achievementRateTxt;
	[SerializeField]
	Sprite[] sprites;

	public void Set (Mission mission, UnityEngine.Events.UnityAction callback) {
		awardImage.sprite = sprites[0];	// とりあえず TODO
		achievementRateTxt.text = mission.achievementRate.ToString("P0");
		this.Regist(mission.data.title, mission.data.summary, callback);
	}
}
