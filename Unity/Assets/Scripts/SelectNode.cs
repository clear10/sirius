﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectNode : MonoBehaviour {

	[SerializeField]
	GameObject back;
	[SerializeField]
	GameObject top;
	[SerializeField]
	float expandRate = 2f;
	[SerializeField]
	LayoutElement layout;
	int flag = 0;

	// Use this for initialization
	void Start () {
	
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.G)) Expand();
		if(Input.GetKeyDown(KeyCode.H)) Close();
	}

	public void Expand () {
		if(flag == 1) return;
		StartCoroutine(ExpandCo(0.2f));
		flag = 1 - flag;
	}

	public void Action () {
		if(flag == 0) Expand();
		else Close();
	}

	public void Regist (string title, string summary, UnityEngine.Events.UnityAction callback) {
		
		GetComponent<Button>().onClick.AddListener((callback == null) ? () => this.Action() : callback);
		back.transform.GetChild(0).GetComponent<Text>().text = summary;
		top.transform.GetChild(0).GetComponent<Text>().text = title;
	}

	IEnumerator ExpandCo (float t) {
		int frame = 20;
		float origin = layout.minHeight;
		layout.minHeight = origin * expandRate;
		back.SetActive(true);
		RectTransform rect = back.GetComponent<RectTransform>();
		float h = origin;
		float y = rect.sizeDelta.y;
		for(int i = 0; i < frame; i++) {
			rect.sizeDelta = new Vector2(rect.sizeDelta.x, Mathf.Lerp(y, h, (float)(i + 1) / (float)frame));
			yield return new WaitForSeconds(t/(float)frame);
		}
	}

	public void Close () {
		if(flag == 0) return;
		layout.minHeight = layout.minHeight / expandRate;
		RectTransform rect = back.GetComponent<RectTransform>();
		rect.sizeDelta = new Vector2(rect.sizeDelta.x, layout.minHeight + rect.anchoredPosition.y);
		back.SetActive(false);
		flag = 1 - flag;
	}
}
