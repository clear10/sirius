﻿using UnityEngine;
using System.Collections;

public class TitleManager : SingletonMonoBehaviour<TitleManager> {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GoSceneBase () {
		UICanvas canvas = UICanvas.Instance;
		canvas.DestroyUnusedUI();
		canvas.CleanPrefabKeys();
		canvas.SetImage(false);
		FadeManager.Instance.LoadLevel("Base", 0.1f);
	}
}
