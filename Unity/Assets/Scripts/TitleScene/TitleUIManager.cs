﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleUIManager : UIController {

	protected override void RegistPrefabs (UICanvas canvas) {
		canvas.ForceDestroy("BasePanel");
		canvas.AddPrefab("CommonButton", "Prefabs/UI/CommonButton", true);
		canvas.AddPrefab("TitleText", "Prefabs/UI/TitleText");
		//canvas.AddPrefab("DebugText", "Prefabs/UI/DebugText");
	}

	protected override void ShowUI (UICanvas canvas, Transform parent) {

		GameObject btn2baseObj = Instantiate(canvas.GetPrefab("CommonButton")) as GameObject;
		TitleManager manager = this.GetComponent<TitleManager>();

		InitButton(btn2baseObj, "Go2Base", parent, () => manager.GoSceneBase());

		GameObject titleTxt = Instantiate(canvas.GetPrefab("TitleText")) as GameObject;
		titleTxt.transform.SetParent(parent, false);

		GameObject debugTxt = DebugText.Instance.gameObject; //Instantiate(canvas.GetPrefab("DebugText")) as GameObject;
		//debugTxt.transform.SetParent(parent, false);
		canvas.AddDontDestroy(debugTxt);
	}
}
