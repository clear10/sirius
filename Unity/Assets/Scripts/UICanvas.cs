﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UICanvas : SingletonMonoBehaviour<UICanvas> {

	Dictionary<string, GameObject> prefabs;
	Dictionary<string, string> resourcePaths;

	List<string> prefabKeys;
	List<string> reuseKeys;
	List<GameObject> dontDestroyObjs;
	RectTransform pnlBase;

	Vector2 size = new Vector2(720, 1230);

	static bool created = false;
	public bool Loaded { get; set; }

	public Transform Base {
		get { if(pnlBase == null) { pnlBase = transform.GetChild(0).GetComponent<RectTransform>(); } return pnlBase.gameObject.transform; }
	}

	public Vector2 Size {
		get { return size; }
	}

	protected override void Awake () {
		base.Awake();
		DebugText.Instance.Log("Canvas awaked");
		//DebugText.Instance.Log("created: " + created);
		if(!created) {
			DontDestroyOnLoad(Instance.gameObject);
			if(prefabKeys == null) prefabKeys = new List<string>();
			if(reuseKeys == null) reuseKeys = new List<string>();
			if(dontDestroyObjs == null) dontDestroyObjs = new List<GameObject>();
			if(prefabs == null) prefabs = new Dictionary<string, GameObject>();
			if(resourcePaths == null) resourcePaths = new Dictionary<string, string>();
			Loaded = false;
			created = true;
			return;
		}
		Destroy(this.gameObject);
	}

	public IEnumerator LoadPrefabs () {
		string s = "OK";
		try {
			foreach(string key in prefabKeys) {
				LoadPrefab(key);
			}
		}
		catch(System.Exception e) {
			s = e.Message;
		}

		Loaded = true;
		yield return s;
	}

	GameObject LoadPrefab (string key) {
		if(prefabs.ContainsKey(key)) return prefabs[key];

		//Debug.Log("key=" + key);
		//foreach(string s in prefabs.Keys) {
		//	Debug.Log("Key: " + s);
		//}
		string path = GetResourcePath(key);
		if(path == null) return null;
		prefabs.Add(key, Resources.Load<GameObject>(path));
		//Debug.Log("LoadPrefab: " + prefabs[key]);
		return prefabs[key];
	}

	string GetResourcePath (string key) {
		if(resourcePaths.ContainsKey(key)) return resourcePaths[key];

		//Debug.Log("key=" + key);
		//foreach(string s in resourcePaths.Keys) {
		//	Debug.Log("Key: " + s);
		//}

		return null;
	}

	public bool AddPrefab (string key, string resourcePath, bool reuseFlag = false) {
		if(prefabKeys.Contains(key)) {
			//DebugText.Instance.Log(key + " already added");
			return false;
		}
		//if(prefabs.ContainsKey(key)) return false;
		//if(resourcePaths.ContainsKey(key)) return false;

		//Debug.Log("AddPrefab: " + key);

		prefabKeys.Add(key);
		resourcePaths.Add(key, resourcePath);
		if(reuseFlag) reuseKeys.Add(key);
		return true;
	}

	public void AddDontDestroy (GameObject obj) {
		//Debug.Log("AddDonstDestroy: " + obj);
		if(!dontDestroyObjs.Contains(obj))
			dontDestroyObjs.Add(obj);
		//else DebugText.Instance.Log("already added: " + obj);
	}

	public GameObject GetPrefab (string key) {
		if(prefabs.ContainsKey(key)) return prefabs[key];

		//Debug.Log("key=" + key);
		//foreach(string s in prefabs.Keys) {
		//	Debug.Log("Key: " + s);
		//}

		//Debug.logWarning("Please load prefab before: "+key);
		return LoadPrefab(key);
	}

	public GameObject GetDontDestroyedObj (string name) {
		return dontDestroyObjs.Find(obj => (obj.name == name));
	}

	public void DestroyUnusedUI () {
		foreach(Transform t in Base) {
			GameObject obj = t.gameObject;
			if(!dontDestroyObjs.Contains(obj)) {
				Destroy(obj);
			}
			else DontDestroyOnLoad(obj);
		}
		foreach(Transform t in transform) {
			GameObject obj = t.gameObject;
			if(Base.gameObject != obj && DebugText.Instance.gameObject != obj) Destroy(obj);
		}
		Resources.UnloadUnusedAssets();
	}

	public void ForceDestroy (string name) {
		GameObject go = GetDontDestroyedObj(name);
		if(go == null) return;

		dontDestroyObjs.Remove(go);
		Destroy(go);
	}

	public void CleanPrefabKeys () {
		for(int i = 0; i < prefabKeys.Count; i++) {
			string s = prefabKeys[i];
			if(reuseKeys.Contains(s)) continue;

			prefabKeys.RemoveAt(i);
			resourcePaths.Remove(s);
			if(prefabs.ContainsKey(s)) prefabs.Remove(s);
			i--;
		}
	}

	public void SetImage (bool flag) {
		Image image = Base.GetComponent<Image>();
		if(image == null) return;
		//image.enabled = flag;
		if(!flag) Destroy(image);
	}

	public ModelTakedOverCanvas CreateModel () {
		return new ModelTakedOverCanvas(this.prefabs, this.resourcePaths, this.prefabKeys, this.reuseKeys, this.dontDestroyObjs);
	}

	public void SetParamByModel (ModelTakedOverCanvas model, RectTransform root) {
		//DebugText.Instance.Log("Canvas SetParamByModel");
		if(instance == null) instance = this;
		this.prefabs = model.Prefabs;
		this.prefabKeys = model.PrefabKeys;
		this.reuseKeys = model.ReuseKeys;
		this.resourcePaths = model.ResourcePaths;
		this.dontDestroyObjs = model.DontDestroyObjs;
		this.pnlBase = root;
		SetImage(false);
		Loaded = false;
	}

	public void CleanAll () {
		prefabKeys = null;
		reuseKeys = null;
		dontDestroyObjs = null;
		prefabs = null;
		resourcePaths = null;
		created = false;
		Resources.UnloadUnusedAssets();

		Awake();
	}

	public void OnDestroy () {
		StopAllCoroutines();
		if(this == UICanvas.Instance) {
			instance = null;
			created = false;
		}
	}
}
