﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UICanvas canvas = UICanvas.Instance;
		RegistPrefabs(canvas);
		StartCoroutine(LoadPrefabs(canvas));
	}

	protected virtual void RegistPrefabs (UICanvas canvas) { }

	IEnumerator LoadPrefabs (UICanvas canvas) {
		var coroutine = canvas.LoadPrefabs();
		yield return StartCoroutine(coroutine);
		var message = coroutine.Current as string;
		DebugText.Instance.Log("Load: " + message);

		if(message == "OK") ShowUI(canvas, canvas.Base);
	}

	protected virtual void ShowUI (UICanvas canvas, Transform parent) {	}

	protected void InitButton (GameObject btnObj, string txt, Transform parent, UnityEngine.Events.UnityAction call) {
		btnObj.transform.SetParent(parent, false);
		Text text = btnObj.transform.GetChild(0).GetComponent<Text>();
		//DebugText.Instance.Log(text.text);
		text.text = txt;
		Button btn = btnObj.GetComponent<Button>();
		if(btn == null) return;
		btn.onClick.RemoveAllListeners();
		if(call != null) btn.onClick.AddListener(call);
	}
	
	protected void SetRectTransform (GameObject obj, Vector2 pos) {
		RectTransform rect = obj.GetComponent<RectTransform>();
		if(rect == null) return;

		SetRectTransform(obj, pos, rect.sizeDelta);
	}

	protected void SetRectTransform (GameObject obj, Vector2 pos, Vector2 size) {
		RectTransform rect = obj.GetComponent<RectTransform>();
		if(rect == null) return;

		rect.anchoredPosition = pos;
		rect.sizeDelta = size;
	}
}
