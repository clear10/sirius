using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class mtb_missions_importer : AssetPostprocessor {
	private static readonly string filePath = "Assets/Resources/Data/mtb_missions.xls";
	private static readonly string exportPath = "Assets/Resources/Data/mtb_missions.asset";
	private static readonly string[] sheetNames = { "Missions", };
	
	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		foreach (string asset in importedAssets) {
			if (!filePath.Equals (asset))
				continue;
				
			Entity_Missions data = (Entity_Missions)AssetDatabase.LoadAssetAtPath (exportPath, typeof(Entity_Missions));
			if (data == null) {
				data = ScriptableObject.CreateInstance<Entity_Missions> ();
				AssetDatabase.CreateAsset ((ScriptableObject)data, exportPath);
				data.hideFlags = HideFlags.NotEditable;
			}
			
			data.sheets.Clear ();
			using (FileStream stream = File.Open (filePath, FileMode.Open, FileAccess.Read)) {
				IWorkbook book = new HSSFWorkbook (stream);
				
				foreach(string sheetName in sheetNames) {
					ISheet sheet = book.GetSheet(sheetName);
					if( sheet == null ) {
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					Entity_Missions.Sheet s = new Entity_Missions.Sheet ();
					s.name = sheetName;
				
					for (int i=2; i<= sheet.LastRowNum; i++) {
						IRow row = sheet.GetRow (i);
						ICell cell = null;
						
						Entity_Missions.Param p = new Entity_Missions.Param ();
						
					cell = row.GetCell(0); p.enable = (cell == null ? false : cell.BooleanCellValue);
					if(!p.enable) continue;
					cell = row.GetCell(1); p.id = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(2); p.title = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(3); p.summary = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(4); p.type = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(5); p.appearance_time = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(6); p.before_mission_id = (int)(cell == null ? -1 : cell.NumericCellValue);
					cell = row.GetCell(7); p.award_type = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(8); p.award_id = (int)(cell == null ? 0 : cell.NumericCellValue);
					cell = row.GetCell(9); p.subject = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(10); p.comparison = (cell == null ? "" : cell.StringCellValue);
					cell = row.GetCell(11); p.value = (int)(cell == null ? 0 : cell.NumericCellValue);
						s.list.Add (p);
					}
					data.sheets.Add(s);
				}
			}

			ScriptableObject obj = AssetDatabase.LoadAssetAtPath (exportPath, typeof(ScriptableObject)) as ScriptableObject;
			EditorUtility.SetDirty (obj);
		}
	}
}
