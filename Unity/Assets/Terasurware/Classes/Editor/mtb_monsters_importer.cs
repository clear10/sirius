using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class mtb_monsters_importer : AssetPostprocessor {
	private static readonly string filePath = "Assets/Resources/Data/mtb_monsters.xls";
	private static readonly string exportPath = "Assets/Resources/Data/mtb_monsters.asset";
	private static readonly string[] sheetNames = { "Monsters", };

	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
		foreach(string asset in importedAssets) {
			if(!filePath.Equals(asset))
				continue;

			Entity_Monsters data = (Entity_Monsters)AssetDatabase.LoadAssetAtPath(exportPath, typeof(Entity_Monsters));
			if(data == null) {
				data = ScriptableObject.CreateInstance<Entity_Monsters>();
				AssetDatabase.CreateAsset((ScriptableObject)data, exportPath);
				data.hideFlags = HideFlags.NotEditable;
			}

			data.sheets.Clear();
			using(FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read)) {
				IWorkbook book = new HSSFWorkbook(stream);

				foreach(string sheetName in sheetNames) {
					ISheet sheet = book.GetSheet(sheetName);
					if(sheet == null) {
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					Entity_Monsters.Sheet s = new Entity_Monsters.Sheet();
					s.name = sheetName;

					for(int i = 2; i <= sheet.LastRowNum; i++) {
						IRow row = sheet.GetRow(i);
						ICell cell = null;

						Entity_Monsters.Param p = new Entity_Monsters.Param();

						cell = row.GetCell(0); p.enable = (cell == null ? false : cell.BooleanCellValue);
						if(!p.enable) continue;
						cell = row.GetCell(1); p.id = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(2); p.name = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(3); p.appearance_time = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(4); p.beforebattle_label = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(5); p.afterbattle_label = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(6); p.filename = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(7); p.regenerate = (cell == null ? false : cell.BooleanCellValue);
						cell = row.GetCell(8); p.hp = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(9); p.atk = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(10); p.agl = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(11); p.reg = (int)(cell == null ? 0 : cell.NumericCellValue);
						s.list.Add(p);
					}
					data.sheets.Add(s);
				}
			}

			ScriptableObject obj = AssetDatabase.LoadAssetAtPath(exportPath, typeof(ScriptableObject)) as ScriptableObject;
			EditorUtility.SetDirty(obj);
		}
	}
}
