using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class mtb_phrases_importer : AssetPostprocessor {
	private static readonly string filePath = "Assets/Resources/Data/mtb_phrases.xls";
	private static readonly string exportPath = "Assets/Resources/Data/mtb_phrases.asset";
	private static readonly string[] sheetNames = { "Phrases", };

	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
		foreach(string asset in importedAssets) {
			if(!filePath.Equals(asset))
				continue;

			Entity_Phrases data = (Entity_Phrases)AssetDatabase.LoadAssetAtPath(exportPath, typeof(Entity_Phrases));
			if(data == null) {
				data = ScriptableObject.CreateInstance<Entity_Phrases>();
				AssetDatabase.CreateAsset((ScriptableObject)data, exportPath);
				data.hideFlags = HideFlags.NotEditable;
			}

			data.sheets.Clear();
			using(FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read)) {
				IWorkbook book = new HSSFWorkbook(stream);

				foreach(string sheetName in sheetNames) {
					ISheet sheet = book.GetSheet(sheetName);
					if(sheet == null) {
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					Entity_Phrases.Sheet s = new Entity_Phrases.Sheet();
					s.name = sheetName;

					for(int i = 2; i < sheet.LastRowNum; i++) {
						IRow row = sheet.GetRow(i);
						ICell cell = null;

						Entity_Phrases.Param p = new Entity_Phrases.Param();

						cell = row.GetCell(0); p.enable = (cell == null ? false : cell.BooleanCellValue);
						if(!p.enable) continue;
						cell = row.GetCell(1); p.id = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(2); p.unit_id = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(3); p.text = (cell == null ? "" : cell.StringCellValue);
						s.list.Add(p);
					}
					data.sheets.Add(s);
				}
			}

			ScriptableObject obj = AssetDatabase.LoadAssetAtPath(exportPath, typeof(ScriptableObject)) as ScriptableObject;
			EditorUtility.SetDirty(obj);
		}
	}
}
