using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public class mtb_units_importer : AssetPostprocessor {
	private static readonly string filePath = "Assets/Resources/Data/mtb_units.xls";
	private static readonly string[] sheetNames = { "Units", };

	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
		foreach(string asset in importedAssets) {
			if(!filePath.Equals(asset))
				continue;

			using(FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read)) {
				var book = new HSSFWorkbook(stream);

				foreach(string sheetName in sheetNames) {
					var exportPath = "Assets/Resources/Data/" + sheetName + ".asset";

					// check scriptable object
					var data = (Entity_Unit)AssetDatabase.LoadAssetAtPath(exportPath, typeof(Entity_Unit));
					if(data == null) {
						data = ScriptableObject.CreateInstance<Entity_Unit>();
						AssetDatabase.CreateAsset((ScriptableObject)data, exportPath);
						data.hideFlags = HideFlags.NotEditable;
					}
					data.param.Clear();

					// check sheet
					var sheet = book.GetSheet(sheetName);
					if(sheet == null) {
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					// add infomation
					for(int i = 2; i <= sheet.LastRowNum; i++) {
						IRow row = sheet.GetRow(i);
						ICell cell = null;

						var p = new Entity_Unit.Param();

						cell = row.GetCell(0); p.enable = (cell == null ? false : cell.BooleanCellValue);
						if(!p.enable) continue;
						cell = row.GetCell(1); p.id = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(2); p.name = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(3); p.job = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(4); p.hp = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(5); p.atk = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(6); p.agl = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(7); p.s_atk = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(8); p.s_def = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(9); p.reg = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(10); p.appearance_time = (int)(cell == null ? 0 : cell.NumericCellValue);

						data.param.Add(p);
					}

					// save scriptable object
					ScriptableObject obj = AssetDatabase.LoadAssetAtPath(exportPath, typeof(ScriptableObject)) as ScriptableObject;
					EditorUtility.SetDirty(obj);
				}
			}

		}
	}
}
