using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_Missions : ScriptableObject
{	
	public List<Sheet> sheets = new List<Sheet> ();

	[System.SerializableAttribute]
	public class Sheet
	{
		public string name = string.Empty;
		public List<Param> list = new List<Param>();
	}

	[System.SerializableAttribute]
	public class Param
	{
		
		public bool enable;
		public int id;
		public string title;
		public string summary;
		public int type;
		public int appearance_time;
		public int before_mission_id;
		public int award_type;
		public int award_id;
		public string subject;
		public string comparison;
		public int value;
	}
}

