using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_Monsters : ScriptableObject
{	
	public List<Sheet> sheets = new List<Sheet> ();

	[System.SerializableAttribute]
	public class Sheet
	{
		public string name = string.Empty;
		public List<Param> list = new List<Param>();
	}

	[System.SerializableAttribute]
	public class Param
	{
		
		public bool enable;
		public int id;
		public string name;
		public int appearance_time;
		public string beforebattle_label;
		public string afterbattle_label;
		public string filename;
		public bool regenerate;
		public int hp;
		public int atk;
		public int agl;
		public int reg;
	}
}

