using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_Unit : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public bool enable;
		public int id;
		public string name;
		public string job;
		public int hp;
		public int atk;
		public int agl;
		public int s_atk;
		public int s_def;
		public int reg;
		public int appearance_time;
	}
}