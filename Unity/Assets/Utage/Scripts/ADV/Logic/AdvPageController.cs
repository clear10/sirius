//----------------------------------------------
// UTAGE: Unity Text Adventure Game Engine
// Copyright 2014 Ryohei Tokimura
//----------------------------------------------

using UnityEngine;

namespace Utage
{

	/// <summary>
	/// ページ制御
	/// </summary>
	public class AdvPageController
	{
		public enum Type
		{
			None,
			KeepText,
		};

		//テキスト表示を続けたままにするか
		bool isKeepText;
		public bool IsKeepText
		{
			get { return isKeepText; }
		}

		public void Update( Type type )
		{
			switch (type)
			{
			case Type.None:
				Clear();
				break;
			case Type.KeepText:
				isKeepText = true;
				break;
			}
		}

		public void Clear()
		{
			isKeepText = false;
		}
	}
}
