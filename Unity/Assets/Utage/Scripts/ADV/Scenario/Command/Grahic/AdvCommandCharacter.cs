//----------------------------------------------
// UTAGE: Unity Text Adventure Game Engine
// Copyright 2014 Ryohei Tokimura
//----------------------------------------------
using UnityEngine;

namespace Utage
{

	/// <summary>
	/// コマンド：キャラクター＆台詞表示
	/// </summary>
	internal class AdvCommandCharacter : AdvCommand
	{

		public AdvCommandCharacter(StringGridRow row, AdvSettingDataManager dataManager)
		{
			string name = AdvParser.ParseCell<string>(row, AdvColumnName.Arg1);
			//表示テクスチャ
			string textureLabel = AdvParser.ParseCellOptional<string>(row, AdvColumnName.Arg2, "");
			//表示レイヤー
			this.layerName = AdvParser.ParseCellOptional<string>(row, AdvColumnName.Arg3, "");
			if (!string.IsNullOrEmpty(layerName) && !dataManager.LayerSetting.Contains(layerName, AdvLayerSettingData.LayerType.Character ))
			{
				Debug.LogError(row.ToErrorString(name + ", " + layerName + " is not contained in layer setting"));
			}

			//表示位置
			float x;
			if (AdvParser.TryParseCell<float>(row, AdvColumnName.Arg4, out x))
			{
				this.x = x;
			}
			float y;
			if (AdvParser.TryParseCell<float>(row, AdvColumnName.Arg5, out y))
			{
				this.y = y;
			}
			//フェード時間
			this.fadeTime = AdvParser.ParseCellOptional<float>(row, AdvColumnName.Arg6, 0.2f);

			//ページコントローラー
			this.pageCtrlType = AdvParser.ParseCellOptional<AdvPageController.Type>(row, AdvColumnName.PageCtrl, AdvPageController.Type.None );
			dataManager.PageContoroller.Update(this.pageCtrlType);

			//テキスト関連
			this.text = AdvParser.ParseCellOptional<string>(row, AdvColumnName.Text, "");
			if (AdvCommand.IsEditorErrorCheck)
			{
				TextData textData = new TextData(text);
				if (!string.IsNullOrEmpty(textData.ErrorMsg))
				{
					Debug.LogError( row.ToErrorString(textData.ErrorMsg) );
				}
			}
			//ボイス
			string voice = AdvParser.ParseCellOptional<string>(row, AdvColumnName.Voice, "");
			int voiceVersion = AdvParser.ParseCellOptional<int>(row, AdvColumnName.VoiceVersion, 0);

			//キャラの表示情報を取得
			this.info = dataManager.CharacterSetting.GetCharacterInfo(name, textureLabel);

			if (!string.IsNullOrEmpty(info.ErrorMsg))
			{
				Debug.LogError(row.ToErrorString(info.ErrorMsg));

			}
			if (!string.IsNullOrEmpty(info.Path))
			{
				textureFile = AddLoadFile(info.Path);
			}


			//サウンドファイル設定
			if (!string.IsNullOrEmpty(voice))
			{
				if (AdvCommand.IsEditorErrorCheck)
				{
				}
				else
				{
					voiceFile = AddLoadFile(dataManager.SettingData.VoiceDirInfo.FileNameToPath(voice));
					if (voiceFile != null) voiceFile.Version = voiceVersion;
					//ストリーミング再生にバグがある模様。途中で無音が混じると飛ばされる？
					//				voiceFile.LoadFlags = AssetFileLoadFlags.Streaming;
				}
			}

			//ページの末端かチェック
			//ページコントローラーでテキストを表示しつづける場合は、ページ末端じゃない
			this.isPageEnd = (text.Length > 0) && !dataManager.PageContoroller.IsKeepText;
		}
		public override void DoCommand(AdvEngine engine)
		{
			//ページコントローラー処理
			engine.Page.Contoller.Update( this.pageCtrlType );

			if (engine.LayerManager.IsEventMode)
			{
			}
			else
			{
				if (info.IsHide)
				{
					engine.LayerManager.CharacterFadeOut(info.Label, fadeTime);
				}
				else
				{
					if( info.IsNonePattern )
					{
						if (!engine.LayerManager.SetCharacterSprite(layerName, info.Label, null, x, y, fadeTime))
						{
							engine.LayerManager.SetCharacterSprite(layerName, info.Label, textureFile, x, y, fadeTime);
						}
					}
					else
					{
						engine.LayerManager.SetCharacterSprite(layerName, info.Label, textureFile, x, y, fadeTime);
					}
				}
			}

			if (null != voiceFile)
			{
				engine.SoundManager.Play(SoundManager.StreamType.Voice, voiceFile, false, true);
			}
			if (text.Length > 0)
			{
				engine.Page.SetCharacterText(text, info.NameText);
				engine.BacklogManager.Add(engine.Page.TextData.NoneMetaString, info.NameText, voiceFile);
			}
		}
		public override bool Wait(AdvEngine engine)
		{
			return engine.Page.IsWaitText;
		}

		//ページ区切りのコマンドか
		public override bool IsTypePageEnd() { return isPageEnd; }

		AdvCharacterInfo info;
//		string name;
//		string nameText;
//		string textureLabel;
		string layerName;
		string text;
		AssetFile textureFile;
		AssetFile voiceFile;
		object x = null;
		object y = null;
		float fadeTime;
		bool isPageEnd;
		AdvPageController.Type pageCtrlType;
	}

}