//----------------------------------------------
// UTAGE: Unity Text Adventure Game Engine
// Copyright 2014 Ryohei Tokimura
//----------------------------------------------

using UnityEngine;
using UnityEngine.EventSystems;


namespace Utage
{
	/// <summary>
	/// UI全般の管理
	/// </summary>
	[AddComponentMenu("Utage/ADV/UiManager")]
	public class AdvUguiManager : AdvUiManager
	{
		[SerializeField]
		AdvUguiMessageWindow messageWindow;

		[SerializeField]
		AdvUguiSelectionManager selection;

		[SerializeField]
		AdvUguiBacklogManager backLog;

		/// <summary>
		/// 初期化。スクリプトから動的に生成する場合に
		/// </summary>
		/// <param name="engine">ADVエンジン</param>
		public void InitOnCreate(AdvEngine engine, AdvUguiMessageWindow messageWindow, AdvUguiSelectionManager selection, AdvUguiBacklogManager backLog)
		{
			this.engine = engine;
			this.messageWindow = messageWindow;
			this.selection = selection;
			this.backLog = backLog;
		}

		public override void Open()
		{
			ChangeStatus(UiStatus.Default);
			this.gameObject.SetActive(true);
		}

		public override void Close()
		{
			this.gameObject.SetActive(false);
			if (messageWindow != null) messageWindow.Close();
			if (selection != null) selection.Close();
			if (backLog != null) backLog.Close();
		}

		protected override void ChangeStatus(UiStatus newStatus)
		{
			switch (newStatus)
			{
				case UiStatus.Backlog:
					if (backLog == null) return;

					if (messageWindow != null) messageWindow.Close();
					if (selection != null) selection.Close();
					if (backLog != null) backLog.Open();
					engine.Config.IsSkip = false;
					break;
				case UiStatus.HideMessageWindow:
					if (messageWindow != null) messageWindow.Close();
					if (selection != null) selection.Close();
					if (backLog != null) backLog.Close();
					engine.Config.IsSkip = false;
					break;
				case UiStatus.Default:
					if (messageWindow != null) messageWindow.Open();
					if (selection != null) selection.Open();
					if (backLog != null) backLog.Close();
					break;
			}
			this.status = newStatus;
		}

		//ウインドウ閉じるボタンが押された
		void OnTapCloseWindow()
		{
			Status = UiStatus.HideMessageWindow;
		}

		void Update()
		{
			switch (engine.UiManager.Status)
			{
				case AdvUiManager.UiStatus.Backlog:
					break;
				case AdvUiManager.UiStatus.HideMessageWindow:	//メッセージウィンドウが非表示
					//右クリック
					if (InputUtil.IsMousceRightButtonDown())
					{	//通常画面に復帰
						engine.UiManager.Status = AdvUiManager.UiStatus.Default;
					}
					else if (InputUtil.IsInputScrollWheelUp())
					{
						//バックログ開く
						engine.UiManager.Status = AdvUiManager.UiStatus.Backlog;
					}
					break;
				case AdvUiManager.UiStatus.Default:
					if (engine.Page.IsWaitPage)
					{	//入力待ち
						if (InputUtil.IsMousceRightButtonDown())
						{	//右クリックでウィンドウ閉じる
							engine.UiManager.Status = AdvUiManager.UiStatus.HideMessageWindow;
						}
						else if (InputUtil.IsInputScrollWheelUp())
						{	//バックログ開く
							engine.UiManager.Status = AdvUiManager.UiStatus.Backlog;
						}
						else
						{
							if ( (engine.Config.IsMouseWheelSendMessage && InputUtil.IsInputScrollWheelDown())
								|| InputUtil.IsInputKeyboadReturnDown())
							{
								//メッセージ送り
								engine.Page.InputSendMessage();
							}
						}
					}
					break;
			}
		}

		/// <summary>
		/// タッチされたとき
		/// </summary>
		public void OnPointerDown(BaseEventData data)
		{
			if ((data as PointerEventData).button != PointerEventData.InputButton.Left) return;

			switch (engine.UiManager.Status)
			{
				case AdvUiManager.UiStatus.Backlog:
					break;
				case AdvUiManager.UiStatus.HideMessageWindow:	//メッセージウィンドウが非表示
					engine.UiManager.Status = AdvUiManager.UiStatus.Default;
					break;
				case AdvUiManager.UiStatus.Default:
					if (engine.Config.IsSkip)
					{
						//スキップ中ならスキップ解除
						engine.Config.ToggleSkip();
					}
					else
					{
						if (engine.Page.IsShowingText)
						{
							if (!engine.Config.IsSkip)
							{
								//文字送り
								engine.Page.InputSendMessage();
							}
						}
						else
						{
							base.OnPointerDown(data as PointerEventData);
						}
					}
					break;
			}
		}
	}
}