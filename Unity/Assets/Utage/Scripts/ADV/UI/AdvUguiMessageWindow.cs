//----------------------------------------------
// UTAGE: Unity Text Adventure Game Engine
// Copyright 2014 Ryohei Tokimura
//----------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using Utage;

/// <summary>
/// メッセージウィドウ処理のサンプル
/// </summary>
[AddComponentMenu("Utage/ADV/UiMessageWindow")]
public class AdvUguiMessageWindow : MonoBehaviour
{
	/// <summary>ADVエンジン</summary>
	[SerializeField]
	AdvEngine engine;

	/// <summary>本文テキスト</summary>
	[SerializeField]
	UguiNovelText text;

	/// <summary>名前表示テキスト</summary>
	[SerializeField]
	Text nameText;

	/// <summary>ウインドウのルート</summary>
	[SerializeField]
	GameObject rootChildren;

	/// <summary>コンフィグの透明度を反映させるUIのルート</summary>
	[SerializeField]
	CanvasGroup transrateMessageWindowRoot;

	/// <summary>改ページ待ちアイコン</summary>
	[SerializeField]
	GameObject iconBrPage;

	[SerializeField]
	bool isLinkPositionIconBrPage = true;

	/// <summary>
	/// ウィンドウのクローズ
	/// </summary>
	public void Close()
	{
		this.gameObject.SetActive(false);
		rootChildren.SetActive(false);
	}

	/// <summary>
	/// ウィンドウのオープン
	/// </summary>
	public void Open()
	{
		this.gameObject.SetActive(true);
		rootChildren.SetActive(false);
	}

	void Update()
	{
		if (engine.UiManager.Status == AdvUiManager.UiStatus.Default)
		{
			rootChildren.SetActive( !engine.UiManager.IsHide );
			if (!engine.UiManager.IsHide)
			{
				//ウィンドのアルファ値反映
				transrateMessageWindowRoot.alpha = engine.Config.MessageWindowAlpha;
				//テキスト表示の反映
				engine.Page.UpdateText();
				nameText.text = engine.Page.NameText;
				text.TextData = engine.Page.TextData;
				text.LengthOfView = engine.Page.CurrentTextLen;
				if (iconBrPage != null)
				{
					iconBrPage.SetActive(engine.Page.IsWaitBrPage);
					//アイコンの場所をテキストの末端にあわせる
					if (isLinkPositionIconBrPage) iconBrPage.transform.localPosition = text.CurrentEndPosition;
				}
			}
			else
			{
				if (iconBrPage != null) iconBrPage.SetActive(false);
			}
		}
	}


	//ウインドウ閉じるボタンが押された
	public void OnTapCloseWindow()
	{
		engine.UiManager.Status = AdvUiManager.UiStatus.HideMessageWindow;
	}

	//バックログボタンが押された
	public void OnTapBackLog()
	{
		engine.UiManager.Status = AdvUiManager.UiStatus.Backlog;
	}
}

