using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Utage
{

	/// <summary>
	/// デバッグ用に、マウスダウン、アウップでUnityエディタをポーズさせる
	/// </summary>
	[AddComponentMenu("Utage/Lib/System UI/DebugPauseEditor")]
	public class DebugPauseEditor : MonoBehaviour
	{
		public bool isPuaseOnMouseDown = false;
		public bool isPuaseOnMouseUp = false;

#if UNITY_EDITOR
		void Update()
		{
			if ( (isPuaseOnMouseDown && Input.GetButtonDown("Fire1") )
				||(isPuaseOnMouseUp && Input.GetButtonUp("Fire1") ) )
			{
				PauseEditor();
			}
		}

		public void PauseEditor()
		{
			UnityEditor.EditorApplication.isPaused = true;
		}
#endif
	}
}