//----------------------------------------------
// UTAGE: Unity Text Adventure Game Engine
// Copyright 2014 Ryohei Tokimura
//----------------------------------------------

using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace Utage
{

	/// <summary>
	/// システム系のUI処理
	/// </summary>
	[AddComponentMenu("Utage/Lib/System UI/SystemUi")]
	public class SystemUi : MonoBehaviour
	{
		/// <summary>
		/// シングルトンなインスタンスの取得
		/// </summary>
		/// <returns></returns>
		public static SystemUi GetInstance()
		{
			return instance;
		}
		static SystemUi instance;


		void Awake()
		{
			if (null == instance)
			{
				instance = this;
			}
			else
			{
				Debug.LogError(LanguageErrorMsg.LocalizeTextFormat(ErrorMsg.SingletonError));
				Destroy(this);
			}
		}

		[SerializeField]
		SystemUiDialog2Button dialogGameExit;

		public CameraManager CameraManager { get { return this.cameraManager ?? (this.cameraManager = FindObjectOfType<CameraManager>() as CameraManager); } }
		[SerializeField]
		CameraManager cameraManager;

		[SerializeField]
		SystemUiDialog1Button dialog1Button;

		[SerializeField]
		SystemUiDialog2Button dialog2Button;

		[SerializeField]
		SystemUiDialog3Button dialog3Button;

		[SerializeField]
		IndicatorIcon indicator;

		/// <summary>
		/// Escapeボタンの入力有効か
		/// </summary>
		public bool IsEnableInputEscape
		{
			get { return isEnableInputEscape; }
			set { isEnableInputEscape = value; }
		}
		[SerializeField]
		bool isEnableInputEscape = true;


		/// <summary>
		/// 1ボタンのダイアログを開く
		/// </summary>
		/// <param name="text">表示テキスト</param>
		/// <param name="buttonText1">ボタン1のテキスト</param>
		/// <param name="target">ボタンを押したときの呼ばれるコールバック</param>
		public void OpenDialog1Button(string text, string buttonText1, UnityAction callbackOnClickButton1)
		{
			dialog1Button.Open(text, buttonText1, callbackOnClickButton1);
		}

		/// <summary>
		///  2ボタンのダイアログを開く
		/// </summary>
		/// <param name="text">表示テキスト</param>
		/// <param name="buttonText1">ボタン1用のテキスト</param>
		/// <param name="buttonText2">ボタン2用のテキスト</param>
		/// <param name="callbackOnClickButton1">ボタン1を押したときの呼ばれるコールバック</param>
		/// <param name="callbackOnClickButton2">ボタン2を押したときの呼ばれるコールバック</param>
		public void OpenDialog2Button(string text, string buttonText1, string buttonText2, UnityAction callbackOnClickButton1, UnityAction callbackOnClickButton2)
		{
			dialog2Button.Open(text, buttonText1, buttonText2, callbackOnClickButton1, callbackOnClickButton2);
		}
		
		/// <summary>
		/// 3ボタンのダイアログを開く
		/// </summary>
		/// <param name="text">表示テキスト</param>
		/// <param name="buttonText1">ボタン1用のテキスト</param>
		/// <param name="buttonText2">ボタン2用のテキスト</param>
		/// <param name="buttonText3">ボタン3用のテキスト</param>
		/// <param name="callbackOnClickButton1">ボタン1を押したときの呼ばれるコールバック</param>
		/// <param name="callbackOnClickButton2">ボタン2を押したときの呼ばれるコールバック</param>
		/// <param name="callbackOnClickButton3">ボタン3を押したときの呼ばれるコールバック</param>
		public void OpenDialog3Button(string text, string buttonText1, string buttonText2, string buttonText3, UnityAction callbackOnClickButton1, UnityAction callbackOnClickButton2, UnityAction callbackOnClickButton3 )
		{
			dialog3Button.Open(text, buttonText1, buttonText2, buttonText3, callbackOnClickButton1, callbackOnClickButton2, callbackOnClickButton3 );
		}

		/// <summary>
		/// はい、いいえダイアログを開く
		/// </summary>
		/// <param name="text">表示テキスト</param>
		/// <param name="target">ボタンを押したときのメッセージの送り先</param>
		/// <param name="callbackOnClickYes">Yesボタンを押したときの呼ばれるコールバック</param>
		/// <param name="callbackOnClickNo">Noボタンを押したときの呼ばれるコールバック</param>
		public void OpenDialogYesNo(string text, UnityAction callbackOnClickYes, UnityAction callbackOnClickNo)
		{
			OpenDialog2Button(text, LanguageSystemText.LocalizeText(SystemText.Yes), LanguageSystemText.LocalizeText(SystemText.No), callbackOnClickYes, callbackOnClickNo);
		}

		/// <summary>
		/// インジケーターの表示開始
		/// 表示要求しているオブジェクトはあちこちから設定できる。
		/// 全ての要求が終了しない限りは表示を続ける
		/// </summary>
		/// <param name="obj">表示を要求してるオブジェクト</param>
		public void StartIndicator(Object obj)
		{
			indicator.StartIndicator(obj);
		}

		/// <summary>
		/// インジケーターの表示終了
		/// 表示要求しているオブジェクトはあちこちから設定できる。
		/// 全ての要求が終了しない限りは表示を続ける
		/// </summary>
		/// <param name="obj">表示を要求していたオブジェクト</param>
		public void StopIndicator(Object obj)
		{
			indicator.StopIndicator(obj);
		}

		void Update()
		{
			//Android版・バックキーでアプリ終了確認
			if (IsEnableInputEscape)
			{
				if (Input.GetKeyDown(KeyCode.Escape))
				{
					IsEnableInputEscape = false;
					dialogGameExit.Open(
						LanguageSystemText.LocalizeText(SystemText.QuitGame),
						LanguageSystemText.LocalizeText(SystemText.Yes),
						LanguageSystemText.LocalizeText(SystemText.No),
						OnDialogExitGameYes, OnDialogExitGameNo
						);
				}
			}
		}

		//ゲーム終了確認「はい」
		void OnDialogExitGameYes()
		{
			IsEnableInputEscape = true;
			StartCoroutine(CoGameExit());
		}
		//ゲーム終了確認「いいえ」
		void OnDialogExitGameNo()
		{
			IsEnableInputEscape = true;
		}

		//ゲーム終了
		IEnumerator CoGameExit()
		{
			if (CameraManager)
			{
				yield return StartCoroutine(CameraManager.CoGameExit());
			}
			Application.Quit();
		}
	}
}