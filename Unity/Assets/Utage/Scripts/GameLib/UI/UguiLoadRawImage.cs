//----------------------------------------------
// UTAGE: Unity Text Adventure Game Engine
// Copyright 2014 Ryohei Tokimura
//----------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Utage
{

	/// <summary>
	/// 動的にロード可能なRawImage
	/// </summary>
	[ExecuteInEditMode]
	[AddComponentMenu("Utage/Lib/UI/LoadRawImage")]
	[RequireComponent(typeof(RawImage))]
	public class UguiLoadRawImage : MonoBehaviour
	{
		[SerializeField]
		string path;

		public string Path
		{
			get { return path; }
			set
			{
				if (path != value)
				{
					LoadTextureFile(value);
				}
			}
		}


		/// <summary>
		/// スプライト
		/// </summary>
		public RawImage RawImage { get { return rawImage ?? (rawImage = GetComponent<RawImage>()); }}
		RawImage rawImage;

		/// <summary>
		/// テクスチャファイル
		/// </summary>
		public AssetFile TextureFile { get { return this.textureFile; } }
		AssetFile textureFile;

		/// <summary>
		/// テクスチャファイルを設定
		/// </summary>
		/// <param name="path">ファイルパス</param>
		public void LoadTextureFile(string path)
		{
			AssetFile file = AssetFileManager.Load(path, this);
			SetTextureFile(file);
			file.Unuse(this);
		}

		/// <summary>
		/// テクスチャファイルを設定
		/// </summary>
		/// <param name="file">テクスチャファイル</param>
		/// <param name="pixelsToUnits">スプライトを作成する際の、座標1.0単位辺りのピクセル数</param>
		public void SetTextureFile(AssetFile file)
		{
			//直前のファイルがあればそれを削除
			ClearTextureFile();
			this.textureFile = file;
			this.path = file.FileName;

			textureFile.AddReferenceComponet(this.gameObject);
			if (textureFile.IsLoadEnd)
			{
				RawImage.texture = textureFile.Texture;
			}
			else
			{
				StartCoroutine(CoWaitTextureFileLoading());
			}
		}

		IEnumerator CoWaitTextureFileLoading()
		{
			while (!textureFile.IsLoadEnd) yield return 0;
			RawImage.texture = textureFile.Texture;
		}

		/// <summary>
		/// テクスチャファイルをクリア
		/// </summary>
		public void ClearTextureFile()
		{
			RawImage.texture = null;
			AssetFileReference reference = this.GetComponent<AssetFileReference>();
			if (reference != null)
			{
				Destroy(reference);
			}
			this.textureFile = null;
		}
	}
}
