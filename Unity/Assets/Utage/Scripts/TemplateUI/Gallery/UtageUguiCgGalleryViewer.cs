//----------------------------------------------
// UTAGE: Unity Text Adventure Game Engine
// Copyright 2014 Ryohei Tokimura
//----------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Utage;

/// <summary>
/// CGギャラリー画面のサンプル
/// </summary>
[AddComponentMenu("Utage/TemplateUI/CgGalleryViewer")]
public class UtageUguiCgGalleryViewer : UguiView, IPointerClickHandler
{
	/// <summary>
	/// ギャラリー選択画面
	/// </summary>
	public UtageUguiGallery gallery;

	/// <summary>
	/// CG表示画面
	/// </summary>
	public UguiLoadRawImage texture;
	/// <summary>ADVエンジン</summary>
	public AdvEngine Engine { get { return this.engine ?? (this.engine = FindObjectOfType<AdvEngine>() as AdvEngine); } }
	[SerializeField]
	AdvEngine engine;

	AdvCgGalleryData data;
	int cuurentIndex = 0;

	/// <summary>
	/// オープンしたときに呼ばれる
	/// </summary>
	public void Open(AdvCgGalleryData data)
	{
		gallery.Sleep();
		this.Open();
		this.data = data;
		this.cuurentIndex = 0;
		LoadCurrentTexture();
	}

	/// <summary>
	/// クローズしたときに呼ばれる
	/// </summary>
	void OnClose()
	{
		texture.ClearTextureFile();
		gallery.WakeUp();
	}

	void Update()
	{
		//右クリックで戻る
		if (InputUtil.IsMousceRightButtonDown())
		{
			Back();
		}
	}


	public void OnPointerClick(PointerEventData eventData)
	{
		++cuurentIndex;
		if (cuurentIndex >= data.NumOpen)
		{
			Back();
			return;
		}
		else
		{
			LoadCurrentTexture();
		}
	}

	void LoadCurrentTexture()
	{
		AdvTextureSettingData textureData = data.GetDataOpened(cuurentIndex);
		texture.LoadTextureFile(Engine.DataManager.SettingDataManager.TextureSetting.LabelToFilePath(textureData.Key));
	}
}
