//----------------------------------------------
// UTAGE: Unity Text Adventure Game Engine
// Copyright 2014 Ryohei Tokimura
//----------------------------------------------

using UnityEngine;
using Utage;
using System.Collections;


/// <summary>
/// タイトル表示のサンプル
/// </summary>
[AddComponentMenu("Utage/TemplateUI/Title")]
public class UtageUguiBoot : UguiView
{
	/// <summary>ADVエンジン</summary>
	public AdvEngine Engine { get { return this.engine ?? (this.engine = FindObjectOfType<AdvEngine>() as AdvEngine); } }
	[SerializeField]
	AdvEngine engine;

	public UguiFadeTextureStream fadeTextureStream;
	
	public UtageUguiTitle title;


	///最初の画面なので自分でオープンする
	public void Start()
	{
		title.gameObject.SetActive(false);
		StartCoroutine(CoUpdate());
	}

	///
	IEnumerator CoUpdate()
	{
		if (fadeTextureStream)
		{
			fadeTextureStream.gameObject.SetActive(true);
			fadeTextureStream.Play();
			while (fadeTextureStream.IsPlaying) yield return 0;
		}
		while (Engine.IsWaitBootLoading) yield return 0;

		this.Close();
		title.Open();
	}

}
